// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtWidgets>

#include <dtkComposer>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkComposerMainWindow : public QMainWindow
{
    Q_OBJECT

public:
     dtkComposerMainWindow(QWidget *parent = 0);
    ~dtkComposerMainWindow(void);

public:
    bool maySave(void);

public:
    void setCurrentFile(const QString& file);

public slots:
    void setModified(bool modified);

public slots:
    bool compositionOpen(void);
    bool compositionOpen(const QString& file);
    bool compositionSave(void);
    bool compositionSaveAs(void);
    bool compositionSaveAs(const QString& file, dtkComposerWriter::Type type = dtkComposerWriter::Ascii);
    bool compositionInsert(void);
    bool compositionInsert(const QString& file);

protected slots:
    void showControls(void);

protected slots:
    void onComposerNodeFlagged(dtkComposerSceneNode *);

protected:
    void closeEvent(QCloseEvent *event);

public:
    dtkComposerWidget *composer;
    dtkComposerControls *controls;
    dtkComposerNodeFactoryView *nodes;
    dtkComposerSceneModel *model;
    dtkComposerSceneNodeEditor *editor;
    dtkComposerSceneView *scene;
    dtkComposerStackView *stack;
    dtkComposerEvaluatorToolBar *bar;

public:
    QMenu *composition_menu;
    QAction *composition_open_action;
    QAction *composition_save_action;
    QAction *composition_saveas_action;
    QAction *composition_insert_action;
    QAction *composition_quit_action;

    QMenu *edit_menu;
    QAction *undo_action;
    QAction *redo_action;

    dtkComposerRecentFilesMenu *recent_compositions_menu;

public:
    QPushButton *compo_button;
    QPushButton *distr_button;
    QPushButton *debug_button;
    QPushButton *view_button;

public:
    bool closing;

public:
    QString current_composition;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkComposerMainWindow::dtkComposerMainWindow(QWidget *parent) : QMainWindow(parent)
{
    // -- Elements

    this->composer = new dtkComposerWidget;

    this->bar = new dtkComposerEvaluatorToolBar(this);
    this->bar->setComposerWidget(this->composer);

    this->controls = nullptr;

    this->editor = new dtkComposerSceneNodeEditor(this);
    this->editor->setScene(this->composer->scene());
    this->editor->setStack(this->composer->stack());
    this->editor->setGraph(this->composer->graph());

    this->model = new dtkComposerSceneModel(this);
    this->model->setScene(this->composer->scene());

    this->scene = new dtkComposerSceneView(this);
    this->scene->setScene(this->composer->scene());
    this->scene->setModel(this->model);

    this->stack = new dtkComposerStackView(this);
    this->stack->setStack(this->composer->stack());

    this->nodes = new dtkComposerNodeFactoryView(this);
    this->nodes->setFactory(this->composer->factory());

    connect(this->composer->scene(), SIGNAL(flagged(dtkComposerSceneNode *)), this, SLOT(onComposerNodeFlagged(dtkComposerSceneNode *)));

    this->closing = false;

    // -- Actions

    this->composition_open_action = new QAction("Open", this);
    this->composition_open_action->setShortcut(QKeySequence::Open);

    this->composition_save_action = new QAction("Save", this);
    this->composition_save_action->setShortcut(QKeySequence::Save);

    this->composition_saveas_action = new QAction("Save As...", this);
    this->composition_saveas_action->setShortcut(QKeySequence::SaveAs);

    this->composition_insert_action = new QAction("Insert", this);
    this->composition_insert_action->setShortcut(Qt::ControlModifier + Qt::ShiftModifier + Qt::Key_I);

    this->undo_action = this->composer->stack()->createUndoAction(this);
    this->undo_action->setShortcut(QKeySequence::Undo);

    this->redo_action = this->composer->stack()->createRedoAction(this);
    this->redo_action->setShortcut(QKeySequence::Redo);

    // -- Menus

    QMenuBar *menu_bar = new QMenuBar(0);

    this->recent_compositions_menu = new dtkComposerRecentFilesMenu("Open recent...", this);

    this->composition_menu = menu_bar->addMenu("Composition");
    this->composition_menu->addAction(this->composition_open_action);
    this->composition_menu->addMenu(this->recent_compositions_menu);
    this->composition_menu->addAction(this->composition_save_action);
    this->composition_menu->addAction(this->composition_saveas_action);
    this->composition_menu->addSeparator();
    this->composition_menu->addAction(this->composition_insert_action);

    this->edit_menu = menu_bar->addMenu("Edit");
    this->edit_menu->addAction(this->composer->view()->searchAction());
    this->edit_menu->addSeparator();
    this->edit_menu->addAction(this->undo_action);
    this->edit_menu->addAction(this->redo_action);
    this->edit_menu->addSeparator();
    this->edit_menu->addAction(this->composer->scene()->flagAsBlueAction());
    this->edit_menu->addAction(this->composer->scene()->flagAsGrayAction());
    this->edit_menu->addAction(this->composer->scene()->flagAsGreenAction());
    this->edit_menu->addAction(this->composer->scene()->flagAsOrangeAction());
    this->edit_menu->addAction(this->composer->scene()->flagAsPinkAction());
    this->edit_menu->addAction(this->composer->scene()->flagAsRedAction());
    this->edit_menu->addAction(this->composer->scene()->flagAsYellowAction());
    this->edit_menu->addAction(this->composer->scene()->setBreakPointAction());
    this->edit_menu->addSeparator();
    this->edit_menu->addAction(this->composer->scene()->maskEdgesAction());
    this->edit_menu->addAction(this->composer->scene()->unmaskEdgesAction());

    QAction *showControlsAction = new QAction("Show controls", this);
    showControlsAction->setShortcut(QKeySequence(Qt::ShiftModifier + Qt::ControlModifier + Qt::AltModifier + Qt::Key_C));

    QMenu *window_menu = menu_bar->addMenu("Window");
    window_menu->addAction(showControlsAction);

    // -- Connections

    connect(showControlsAction, SIGNAL(triggered()), this, SLOT(showControls()));

    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(close()));

    connect(this->composer, SIGNAL(modified(bool)), this, SLOT(setModified(bool)));

    connect(this->composition_open_action, SIGNAL(triggered()), this, SLOT(compositionOpen()));
    connect(this->composition_save_action, SIGNAL(triggered()), this, SLOT(compositionSave()));
    connect(this->composition_saveas_action, SIGNAL(triggered()), this, SLOT(compositionSaveAs()));
    connect(this->composition_insert_action, SIGNAL(triggered()), this, SLOT(compositionInsert()));

    connect(this->recent_compositions_menu, SIGNAL(recentFileTriggered(const QString&)), this, SLOT(compositionOpen(const QString&)));

    // -- Layout

    QSplitter *right = new QSplitter(this);
    right->setHandleWidth(2);
    right->setOrientation(Qt::Vertical);
    right->addWidget(this->scene);
    right->addWidget(this->editor);
    right->addWidget(this->stack);
    right->addWidget(this->composer->compass());
    right->setSizes(QList<int>()
                    << this->size().height()/4
                    << this->size().height()/4
                    << this->size().height()/4
                    << this->size().height()/4);

    int wl = 300;
    int wr = 300;
    int wc = 600;

    QSplitter *inner = new QSplitter(this);
    inner->setHandleWidth(2);
    inner->setOrientation(Qt::Horizontal);
    inner->addWidget(this->nodes);
    inner->addWidget(this->composer);
    inner->addWidget(right);
    inner->setSizes(QList<int>() << wl << wc << wr);

    QVBoxLayout* main_layout = new QVBoxLayout;
    main_layout->setContentsMargins(0, 0, 0, 0);
    main_layout->addWidget(this->bar);
    main_layout->addWidget(inner);

    QWidget *central = new QWidget(this);
    central->setLayout(main_layout);

    this->setCentralWidget(central);
    this->setCurrentFile("");

    // Set up composition workspace.

    this->composer->setVisible(true);
    this->composer->compass()->setVisible(true);
    this->nodes->setVisible(true);
    this->scene->setVisible(true);
    this->editor->setVisible(true);
    this->stack->setVisible(false);
}

dtkComposerMainWindow::~dtkComposerMainWindow(void)
{

}

bool dtkComposerMainWindow::maySave(void)
{
    if(this->closing)
        return true;

    if (isWindowModified()) {
        QMessageBox msgBox;
        msgBox.setWindowTitle("gnomon");
        msgBox.setText("The composition has been modified.");
        msgBox.setInformativeText("Do you want to save your changes?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        msgBox.setStyleSheet("");
        int ret = msgBox.exec();

        if (ret == QMessageBox::Save)
            return compositionSave();
        else
            if(ret == QMessageBox::Cancel)
                return false;
    }

    return true;
}

void dtkComposerMainWindow::setCurrentFile(const QString &file)
{
     this->current_composition = file;

     setWindowModified(false);

     QString shownName = this->current_composition;

     if (shownName.isEmpty())
         shownName = "untitled.dtk";

     setWindowFilePath(shownName);
}

void dtkComposerMainWindow::setModified(bool modified)
{
    setWindowModified(modified);
}

bool dtkComposerMainWindow::compositionOpen(void)
{
    if(!this->maySave())
        return true;

    QString path;

    QSettings settings("inria", "dtk");
    settings.beginGroup("VisualProgramming");
    settings.beginGroup("editor");
    path = settings.value("last_open_composition_path").toString();
    settings.endGroup();

    QFileDialog *dialog = new QFileDialog(this, tr("Open composition"), path, QString("dtk composition (*.dtk)"));
    dialog->setStyleSheet("");
    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::AnyFile);
    dialog->open(this, SLOT(compositionOpen(const QString&)));

    return true;
}

bool dtkComposerMainWindow::compositionOpen(const QString& file)
{
    if(sender() == this->recent_compositions_menu && !this->maySave())
        return true;

    bool status = this->composer->open(file);

    if(status) {
        this->recent_compositions_menu->addRecentFile(file);
        this->setCurrentFile(file);
    }

    QFileInfo info(file);

    QSettings settings("inria", "dtk");
    settings.beginGroup("VisualProgramming");
    settings.setValue("last_open_composition_path", info.absolutePath());
    settings.endGroup();

    return status;
}

bool dtkComposerMainWindow::compositionSave(void)
{
    bool status;

    if(this->current_composition.isEmpty() || this->current_composition == "untitled.dtk")
        status = this->compositionSaveAs();
    else
        status = this->composer->save();

    if(status)
        this->setWindowModified(false);

    if(status)
        qDebug() << QString("<div style=\"color: #006600\">Saved %1</div>").arg(this->current_composition);

    return status;
}

bool dtkComposerMainWindow::compositionSaveAs(void)
{
    bool status = false;

    QSettings settings("inria", "dtk");
    settings.beginGroup("VisualProgramming");
    QString path = settings.value("last_open_dir", QDir::homePath()).toString();
    settings.endGroup();

    QStringList nameFilters;
    nameFilters <<  "Ascii composition (*.dtk)";
    nameFilters << "Binary composition (*.dtk)";

    QFileDialog dialog(this, "Save composition as ...", path, QString("dtk composition (*.dtk)"));
    dialog.setStyleSheet("");
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setConfirmOverwrite(true);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilters(nameFilters);
    dialog.setDefaultSuffix("dtk");

    if(dialog.exec()) {

        if(dialog.selectedNameFilter() == nameFilters.at(0))
            status = this->compositionSaveAs(dialog.selectedFiles().first(), dtkComposerWriter::Ascii);
        else
            status = this->compositionSaveAs(dialog.selectedFiles().first(), dtkComposerWriter::Binary);
    }

    return status;
}

bool dtkComposerMainWindow::compositionSaveAs(const QString& file, dtkComposerWriter::Type type)
{
    bool status = false;

    if(file.isEmpty())
        return status;

    status = this->composer->save(file, type);

    if(status) {
        this->setCurrentFile(file);
        this->setWindowModified(false);
    }

    QFileInfo info(file);

    QSettings settings("inria", "dtk");
    settings.beginGroup("VisualProgramming");
    settings.setValue("last_open_dir", info.absolutePath());
    settings.endGroup();

    if(status)
        qDebug() << QString("<div style=\"color: #006600\">Saved as %1</div>").arg(info.baseName());

    return status;
}

bool dtkComposerMainWindow::compositionInsert(void)
{
    QSettings settings("inria", "dtk");
    settings.beginGroup("VisualProgramming");
    QString path = settings.value("last_open_dir", QDir::homePath()).toString();
    settings.endGroup();

    QFileDialog *dialog = new QFileDialog(this, tr("Insert composition"), path, QString("dtk composition (*.dtk)"));
    dialog->setStyleSheet("");
    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::AnyFile);
    dialog->open(this, SLOT(compositionInsert(const QString&)));

    return true;
}

bool dtkComposerMainWindow::compositionInsert(const QString& file)
{
    bool status = this->composer->insert(file);

    if(status)
        this->setWindowModified(true);

    QFileInfo info(file);

    QSettings settings("inria", "dtk");
    settings.beginGroup("VisualProgramming");
    settings.setValue("last_open_dir", info.absolutePath());
    settings.endGroup();

    return status;
}

void dtkComposerMainWindow::showControls(void)
{
    if(!this->controls) {
        this->controls = new dtkComposerControls(this);
        this->controls->setScene(this->composer->scene());
        this->controls->setWindowFlags(Qt::Dialog);
        this->controls->setWindowTitle("Composer Controls");

        if(!this->isFullScreen()) {
            this->controls->resize(this->controls->size().width(), this->size().height());
            this->controls->move(this->rect().topRight() + QPoint(10, 0));
        }
    }

    this->controls->show();
}

void dtkComposerMainWindow::closeEvent(QCloseEvent *event)
{
    if (this->maySave()) {
         this->closing = true;
         event->accept();
     } else {
         event->ignore();
     }
}

void dtkComposerMainWindow::onComposerNodeFlagged(dtkComposerSceneNode *node)
{
    dtkComposerViewController::instance()->insert(node);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QApplication application(argc, argv);

    dtkComposer::node::initialize();
    dtkComposer::extension::activateObjectManager();
    dtkComposer::extension::initialize();

    dtkComposerMainWindow *window = new dtkComposerMainWindow;
    window->show();
    window->raise();

    return application.exec();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
