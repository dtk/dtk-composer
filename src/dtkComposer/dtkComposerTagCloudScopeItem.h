// Version: $Id: b7a25c68f78b741d082724f9c9a2ec469dc17c69 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport.h>
#include <QtWidgets>

class dtkComposerTagCloudScopeItemPrivate;

class DTKCOMPOSER_EXPORT dtkComposerTagCloudScopeItem : public QWidget
{
    Q_OBJECT

public:
     dtkComposerTagCloudScopeItem(QWidget *parent = 0);
    ~dtkComposerTagCloudScopeItem(void);

signals:
    void clicked(void);

public:
    QSize sizeHint(void) const;

public:
    QString text(void);

public:
    void setDark(void);
    void setDoom(void);

public slots:
    void setText(const QString& text);
    void setCount(int count);

protected:
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);

protected:
    void mouseReleaseEvent(QMouseEvent *);

protected:
    dtkComposerTagCloudScopeItemPrivate *d;
};

//
// dtkComposerTagCloudScopeItem.h ends here
