// Version: $Id:
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtWidgets>

#include <dtkComposerExport.h>

class DTKCOMPOSER_EXPORT dtkComposerRecentFilesMenu : public QMenu
{
    Q_OBJECT
    Q_PROPERTY(int maxCount READ maxCount WRITE setMaxCount)

public:
    dtkComposerRecentFilesMenu(QWidget *parent = 0);
    dtkComposerRecentFilesMenu(const QString& title, QWidget *parent = 0);

    int maxCount(void) const;

public slots:
    void addRecentFile(const QString& fileName);
    void clearMenu(void);
    void setMaxCount(int);

signals:
    void recentFileTriggered(const QString& filename);

private slots:
    void menuTriggered(QAction *action);
    void updateRecentFileActions(void);

private:
    int m_maxCount;
};

//
// dtkComposerRecentFilesMenu.h ends here
