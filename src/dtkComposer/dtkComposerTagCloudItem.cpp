// Version: $Id: 5e25db8dc81092ec13d3cbdd2f1937b30c173ca3 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerTagCloudItem.h"

class dtkComposerTagCloudItemPrivate
{
public:
    QString name;
    QString description;
    QStringList tags;
    QString kind;
    QString type;
};

dtkComposerTagCloudItem::dtkComposerTagCloudItem(QString name) : QListWidgetItem(name)
{
    d = new dtkComposerTagCloudItemPrivate;
    d->name = name;
}

dtkComposerTagCloudItem::dtkComposerTagCloudItem(QString name, QString description) : QListWidgetItem(name)
{
    d = new dtkComposerTagCloudItemPrivate;
    d->name = name;
    d->description = description;
}

dtkComposerTagCloudItem::dtkComposerTagCloudItem(QString name, QString description, QStringList tags) : QListWidgetItem(name)
{
    d = new dtkComposerTagCloudItemPrivate;
    d->name = name;
    d->description = description;
    d->tags = tags;
}

dtkComposerTagCloudItem::dtkComposerTagCloudItem(QString name, QString description, QStringList tags, QString kind, QString type) : QListWidgetItem(name)
{
    d = new dtkComposerTagCloudItemPrivate;
    d->name = name;
    d->description = description;
    d->tags = tags;
    d->kind = kind;
    d->type = type;
}

dtkComposerTagCloudItem::dtkComposerTagCloudItem(const dtkComposerTagCloudItem& item) : QListWidgetItem(item.name())
{
    d = new dtkComposerTagCloudItemPrivate;
    d->name = item.d->name;
    d->description = item.d->description;
    d->tags = item.d->tags;
    d->kind = item.d->kind;
    d->type = item.d->type;
}

dtkComposerTagCloudItem::~dtkComposerTagCloudItem(void)
{
    delete d;
}

QString dtkComposerTagCloudItem::name(void) const
{
    return d->name;
}

QString dtkComposerTagCloudItem::description(void) const
{
    return d->description;
}

QStringList dtkComposerTagCloudItem::tags(void) const
{
    return d->tags;
}

QString dtkComposerTagCloudItem::kind(void) const
{
    return d->kind;
}

QString dtkComposerTagCloudItem::type(void) const
{
    return d->type;
}

//
// dtkComposerTagCloudItem.cpp ends here
