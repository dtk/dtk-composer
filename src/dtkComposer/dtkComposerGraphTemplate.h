// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkComposerNodeGraphBase.h"

template <typename N, typename E>
using dtkComposerGraphTemplate = dtkComposerNodeGraphBase<N, E>;

//
// dtkComposerGraphTemplate.h ends here
