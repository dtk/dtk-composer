// Version: $Id: 3860690ee28d2bdfc43d95282fa50f66c001cd7e $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerTagCloudList.h"
#include "dtkComposerTagCloudItem.h"

// ///////////////////////////////////////////////////////////////////
// dtkComposerTagCloudListLightDelegate
// ///////////////////////////////////////////////////////////////////

class dtkComposerTagCloudListLightDelegate: public QStyledItemDelegate
{
public:
    dtkComposerTagCloudListLightDelegate(dtkComposerTagCloudList *list);

public:
    virtual void paint(QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;

public:
    virtual QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;

protected:
    dtkComposerTagCloudList *list;
};

// /////////////////////////////////////////////////////////////////
// dtkComposerTagCloudListDarkDelegate
// /////////////////////////////////////////////////////////////////

class dtkComposerTagCloudListDarkDelegate: public QStyledItemDelegate
{
public:
    dtkComposerTagCloudListDarkDelegate(dtkComposerTagCloudList *list);

public:
    virtual void paint(QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;

public:
    virtual QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;

protected:
    dtkComposerTagCloudList *list;
};

// /////////////////////////////////////////////////////////////////
// dtkComposerTagCloudListDoomDelegate
// /////////////////////////////////////////////////////////////////

class dtkComposerTagCloudListDoomDelegate: public QStyledItemDelegate
{
public:
    dtkComposerTagCloudListDoomDelegate(dtkComposerTagCloudList *list);

public:
    virtual void paint(QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;

public:
    virtual QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;

protected:
    dtkComposerTagCloudList *list;
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerTagCloudListPrivate
// ///////////////////////////////////////////////////////////////////

class dtkComposerTagCloudListPrivate
{
public:
    QList<dtkComposerTagCloudItem *> items;
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerTagCloudList
// ///////////////////////////////////////////////////////////////////

dtkComposerTagCloudList::dtkComposerTagCloudList(QWidget *parent) : QListWidget(parent)
{
    d = new dtkComposerTagCloudListPrivate;

    this->setAttribute(Qt::WA_MacShowFocusRect, false);
    this->setFrameShape(QFrame::NoFrame);
    this->setDragEnabled(true);
    this->setItemDelegate(new dtkComposerTagCloudListLightDelegate(this));
    this->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

    connect(this, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(onItemClicked(QListWidgetItem *)));
}

dtkComposerTagCloudList::~dtkComposerTagCloudList(void)
{
    delete d;
}

void dtkComposerTagCloudList::addItem(QString name)
{
    d->items << new dtkComposerTagCloudItem(name, QString(), QStringList());

    QListWidget::addItem(d->items.last());
}

void dtkComposerTagCloudList::addItem(QString name, QString description)
{
    d->items << new dtkComposerTagCloudItem(name, description, QStringList());

    QListWidget::addItem(d->items.last());
}

void dtkComposerTagCloudList::addItem(QString name, QString description, QStringList tags)
{
    d->items << new dtkComposerTagCloudItem(name, description, tags);

    QListWidget::addItem(d->items.last());
}

void dtkComposerTagCloudList::addItem(QString name, QString description, QStringList tags, QString kind, QString type)
{
    d->items << new dtkComposerTagCloudItem(name, description, tags, kind, type);

    QListWidget::addItem(d->items.last());
}

void dtkComposerTagCloudList::addItem(dtkComposerTagCloudItem item)
{
    d->items << new dtkComposerTagCloudItem(item);

    QListWidget::addItem(d->items.last());
}

void dtkComposerTagCloudList::clear(void)
{
    d->items.clear();

    QListWidget::clear();
}

void dtkComposerTagCloudList::setDark(void)
{
    this->setItemDelegate(new dtkComposerTagCloudListDarkDelegate(this));
}

void dtkComposerTagCloudList::setDoom(void)
{
    this->setItemDelegate(new dtkComposerTagCloudListDoomDelegate(this));
}

void dtkComposerTagCloudList::onItemClicked(QListWidgetItem *item)
{
    dtkComposerTagCloudItem *i = dynamic_cast<dtkComposerTagCloudItem *>(item);

    if (!i)
        return;

    emit itemClicked(i->description());
}

QMimeData *dtkComposerTagCloudList::mimeData(const QList<QListWidgetItem *> items) const
{
    QMimeData *data = NULL;

    dtkComposerTagCloudItem *i = dynamic_cast<dtkComposerTagCloudItem *>(items.first());

    if (i) {
        data = new QMimeData;
        data->setUrls(QList<QUrl>() << QUrl(QString("%1:%2").arg(i->kind()).arg(i->type())));
    }

    return data;
}

QStringList dtkComposerTagCloudList::mimeTypes(void) const
{
    return QStringList() << "text/uri-list";
}

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

QString dtkComposerTagCloudListDelegateUnhtmlize(const QString& htmlString)
{
    QString textString;

    QXmlStreamReader xml(htmlString);

    while (!xml.atEnd())
        if (xml.readNext() == QXmlStreamReader::Characters)
            textString += xml.text();

    return textString;
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerTagCloudListLightDelegate
// ///////////////////////////////////////////////////////////////////

dtkComposerTagCloudListLightDelegate::dtkComposerTagCloudListLightDelegate(dtkComposerTagCloudList *list) : QStyledItemDelegate(list)
{
    this->list = list;
}

void dtkComposerTagCloudListLightDelegate::paint(QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    dtkComposerTagCloudItem *item = dynamic_cast<dtkComposerTagCloudItem *>(list->itemFromIndex(index));

    if (!item)
        return;

    painter->fillRect(option.rect, QColor("#efefef"));
    painter->setPen(Qt::white);
    painter->drawLine(option.rect.topLeft() + QPoint(0, 1), option.rect.topRight() + QPoint(0, 1));

    static QPixmap arrow = QPixmap(":dtkComposerTagCloud/dtkComposerTagCloudList-arrow.png");
    static QPixmap tags = QPixmap(":dtkComposerTagCloud/dtkComposerTagCloudList-tags.png");

    static int m  =  5;
    static int h1 = 20;
    static int h2 = 20;
    static int h3 = 20;

    int w = option.rect.width();
    int h = option.rect.height();
    int t = option.rect.top();
    int r = option.rect.right();

    QRect name_rect = QRect(m, t + 1 * m,           w - 2 * m, h1);
    QRect desc_rect = QRect(m, t + 2 * m + h1,      w - 6 * m, h2);
    QRect tags_rect = QRect(m, t + 3 * m + h1 + h2, w - 2 * m, h3);

    QFontMetrics metrics = QFontMetrics(painter->font());

    painter->setPen(Qt::black);
    painter->drawText(name_rect, Qt::AlignLeft | Qt::AlignTop, item->name());

    painter->setPen(Qt::gray);
    painter->drawText(desc_rect, Qt::AlignLeft | Qt::AlignTop, metrics.elidedText(dtkComposerTagCloudListDelegateUnhtmlize(item->description()), Qt::ElideRight, desc_rect.width()));

    painter->setPen(QColor("#6a769d"));
    painter->drawText(tags_rect.adjusted(m + tags.width(), 0, -tags.width(), 0), Qt::AlignLeft | Qt::AlignTop, item->tags().join(", "));

    painter->setPen(Qt::darkGray);
    painter->drawLine(option.rect.bottomLeft(), option.rect.bottomRight());

    QPointF arrow_pos = QPointF(r - m - arrow.width(), t + h / 2 - arrow.height() / 2);
    painter->drawPixmap(arrow_pos, arrow);
    painter->drawPixmap(tags_rect.topLeft(), tags);
}

QSize dtkComposerTagCloudListLightDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    return QSize(100, 80);
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerTagCloudListDarkDelegate
// ///////////////////////////////////////////////////////////////////

dtkComposerTagCloudListDarkDelegate::dtkComposerTagCloudListDarkDelegate(dtkComposerTagCloudList *list) : QStyledItemDelegate(list)
{
    this->list = list;
}

void dtkComposerTagCloudListDarkDelegate::paint(QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    dtkComposerTagCloudItem *item = dynamic_cast<dtkComposerTagCloudItem *>(list->itemFromIndex(index));

    if (!item)
        return;

    painter->fillRect(option.rect, QColor(020, 020, 020));
    painter->setPen(QColor(055, 055, 055));
    painter->drawLine(option.rect.topLeft() + QPoint(0, 1), option.rect.topRight() + QPoint(0, 1));

    static QPixmap arrow = QPixmap(":dtkComposerTagCloud/dtkComposerTagCloudList-arrow.png");
    static QPixmap tags = QPixmap(":dtkComposerTagCloud/dtkComposerTagCloudList-tags.png");

    static int m  =  5;
    static int h1 = 20;
    static int h2 = 20;
    static int h3 = 20;

    int w = option.rect.width();
    int h = option.rect.height();
    int t = option.rect.top();
    int r = option.rect.right();

    QRect name_rect = QRect(m, t + 1 * m,           w - 2 * m, h1);
    QRect desc_rect = QRect(m, t + 2 * m + h1,      w - 6 * m, h2);
    QRect tags_rect = QRect(m, t + 3 * m + h1 + h2, w - 2 * m, h3);

    QFontMetrics metrics = QFontMetrics(painter->font());

    painter->setPen(Qt::lightGray);
    painter->drawText(name_rect, Qt::AlignLeft | Qt::AlignTop, item->name());

    painter->setPen(Qt::darkGray);
    painter->drawText(desc_rect, Qt::AlignLeft | Qt::AlignTop, metrics.elidedText(dtkComposerTagCloudListDelegateUnhtmlize(item->description()), Qt::ElideRight, desc_rect.width()));

    painter->setPen(QColor("#6a769d"));
    painter->drawText(tags_rect.adjusted(m + tags.width(), 0, -tags.width(), 0), Qt::AlignLeft | Qt::AlignTop, item->tags().join(", "));

    painter->setPen(Qt::black);
    painter->drawLine(option.rect.bottomLeft(), option.rect.bottomRight());

    QPointF arrow_pos = QPointF(r - m - arrow.width(), t + h / 2 - arrow.height() / 2);
    painter->drawPixmap(arrow_pos, arrow);
    painter->drawPixmap(tags_rect.topLeft(), tags);
}

QSize dtkComposerTagCloudListDarkDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    return QSize(100, 80);
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerTagCloudListDoomDelegate
// ///////////////////////////////////////////////////////////////////

dtkComposerTagCloudListDoomDelegate::dtkComposerTagCloudListDoomDelegate(dtkComposerTagCloudList *list) : QStyledItemDelegate(list)
{
    this->list = list;
}

void dtkComposerTagCloudListDoomDelegate::paint(QPainter *painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    dtkComposerTagCloudItem *item = dynamic_cast<dtkComposerTagCloudItem *>(list->itemFromIndex(index));

    if (!item)
        return;

    painter->fillRect(option.rect, QColor("#282C34"));
    painter->setPen(QColor(055, 055, 055));
    painter->drawLine(option.rect.topLeft() + QPoint(0, 1), option.rect.topRight() + QPoint(0, 1));

    static QPixmap arrow = QPixmap(":dtkComposerTagCloud/dtkComposerTagCloudList-arrow.png");
    static QPixmap tags = QPixmap(":dtkComposerTagCloud/dtkComposerTagCloudList-tags.png");

    static int m  =  5;
    static int h1 = 20;
    static int h2 = 20;
    static int h3 = 20;

    int w = option.rect.width();
    int h = option.rect.height();
    int t = option.rect.top();
    int r = option.rect.right();

    QRect name_rect = QRect(m, t + 1 * m,           w - 2 * m, h1);
    QRect desc_rect = QRect(m, t + 2 * m + h1,      w - 6 * m, h2);
    QRect tags_rect = QRect(m, t + 3 * m + h1 + h2, w - 2 * m, h3);

    QFontMetrics metrics = QFontMetrics(painter->font());

    painter->setPen(Qt::lightGray);
    painter->drawText(name_rect, Qt::AlignLeft | Qt::AlignTop, item->name());

    painter->setPen(Qt::darkGray);
    painter->drawText(desc_rect, Qt::AlignLeft | Qt::AlignTop, metrics.elidedText(dtkComposerTagCloudListDelegateUnhtmlize(item->description()), Qt::ElideRight, desc_rect.width()));

    painter->setPen(QColor("#6a769d"));
    painter->drawText(tags_rect.adjusted(m + tags.width(), 0, -tags.width(), 0), Qt::AlignLeft | Qt::AlignTop, item->tags().join(", "));

    painter->setPen(Qt::black);
    painter->drawLine(option.rect.bottomLeft(), option.rect.bottomRight());

    QPointF arrow_pos = QPointF(r - m - arrow.width(), t + h / 2 - arrow.height() / 2);
    painter->drawPixmap(arrow_pos, arrow);
    painter->drawPixmap(tags_rect.topLeft(), tags);
}

QSize dtkComposerTagCloudListDoomDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    return QSize(100, 80);
}

//
// dtkComposerTagCloudList.cpp ends here
