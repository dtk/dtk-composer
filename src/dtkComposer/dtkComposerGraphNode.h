/* dtkComposerGraphNode.h ---
 *
 * Author: Julien Wintz
 * Copyright (C) 2008-2011 - Julien Wintz, Inria.
 * Created: Thu Feb  9 15:08:41 2012 (+0100)
 * Version: $Id: 58a42fa6895c308d5c58876fbd238145ce57e53b $
 * Last-Updated: Thu Apr 11 10:22:11 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 162
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#pragma once

#include <QtCore>

// /////////////////////////////////////////////////////////////////
// dtkComposerGraphNodeList alias
// /////////////////////////////////////////////////////////////////

using dtkComposerGraphNodeList = QList<class dtkComposerGraphNode *>;

// /////////////////////////////////////////////////////////////////
// dtkComposerGraphNode
// /////////////////////////////////////////////////////////////////

class dtkComposerGraphNode : public QObject
{
    Q_OBJECT

public:
     dtkComposerGraphNode(void);
    ~dtkComposerGraphNode(void);

    enum Status
        { Ready,
          Done,
          Running,
          Break };

    enum Kind
        { SelectBranch,
          Leaf,
          Data,
          Process,
          View,
          Actor,
          Begin,
          BeginLoop,
          BeginComposite,
          BeginIf,
          End,
          SetOutputs,
          SetInputs,
          SetVariables,
          SetConditions };

    virtual Kind kind(void) = 0;

    void setStatus(Status status);
    Status status(void) const;

    virtual void clean(void);

    virtual class dtkComposerNode *wrapee(void);

    void setTitle(const QString& title);
    const QString& title(void) const;

    void    addChild(dtkComposerGraphNode *node);
    void removeChild(dtkComposerGraphNode *node);

    void    addPredecessor(dtkComposerGraphNode *node);
    void removePredecessor(dtkComposerGraphNode *node);

    virtual void    addSuccessor(dtkComposerGraphNode *node, int id = 0);
    virtual void removeSuccessor(dtkComposerGraphNode *node);

    void setGraph(class dtkComposerGraph *graph);
    dtkComposerGraph *graph(void);

    void setBreakPoint(bool value = true);
    void setEndLoop(bool value = true);

    bool breakpoint(void) const;
    bool endloop(void) const;

    virtual dtkComposerGraphNodeList successors(void);
    virtual dtkComposerGraphNode    *firstSuccessor(void);
    virtual dtkComposerGraphNodeList predecessors(void);
    virtual dtkComposerGraphNodeList evaluableChilds(void);

    dtkComposerGraphNodeList childs(void);

public slots:
    virtual void eval(void);

private:
    class dtkComposerGraphNodePrivate *d;
};
