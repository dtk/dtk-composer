/* dtkComposerTag.h ---
 *
 * Author: Julien Wintz
 * Created: Mon Apr 15 11:51:10 2013 (+0200)
 */

/* Change Log:
 *
 */

#pragma once

#include <dtkComposerExport.h>

#include <QtWidgets>

class dtkComposerTagPrivate;

class DTKCOMPOSER_EXPORT dtkComposerTag
{
public:
    dtkComposerTag(void);
    dtkComposerTag(QString text, int instances);
    dtkComposerTag(QString text, int instances, QStringList items);
    dtkComposerTag(QString text, int instances, QStringList items, QString color);
    dtkComposerTag(const dtkComposerTag& other);
    ~dtkComposerTag(void);

    int count(void) const;
    QString text(void) const;
    QString color(void) const;
    QStringList items(void) const;

    void setCount(int count);
    void setText(QString text);
    void setColor(QString color);
    void setItems(QStringList items);

protected:
    dtkComposerTagPrivate *d;
};
