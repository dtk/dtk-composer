// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodeControlForEach.h"

#include "dtkComposerConfig.h"
#include "dtkComposerEvaluator.h"
#include "dtkComposerGraph.h"
#include "dtkComposerGraphNodeBeginForEach.h"
#include "dtkComposerGraphNode.h"
#include "dtkComposerNode.h"
#include "dtkComposerNodeControl.h"

#include <dtkLog>

class dtkComposerGraphNodeBeginForEachPrivate
{
public:
    dtkComposerNodeControl *control_node;

    QTime time;
};

dtkComposerGraphNodeBeginForEach::dtkComposerGraphNodeBeginForEach(dtkComposerNode *cnode, const QString& title) : dtkComposerGraphNodeBegin(cnode, title),d(new dtkComposerGraphNodeBeginForEachPrivate)
{
    d->control_node = dynamic_cast<dtkComposerNodeControl *>(cnode);

    this->setTitle(title);
}

QTime dtkComposerGraphNodeBeginForEach::startTime(void)
{
    return d->time;
}

void dtkComposerGraphNodeBeginForEach::eval(void)
{
    d->control_node->begin();

    dtkComposerEvaluator evaluator ;

    evaluator.setGraph(dtkComposerGraphNode::graph());
    evaluator.setStartNode(dtkComposerGraphNode::successors().first());
    evaluator.setEndNode(dtkComposerGraphNodeBegin::end());
    evaluator.setNotify(false);
    this->setStatus(dtkComposerGraphNode::Done);

    d->control_node->setInputs();
    qlonglong size = dynamic_cast<dtkComposerNodeControlForEach *>(d->control_node)->size();
    for (qlonglong i = 0; i < size ; ++i) {
        d->control_node->setVariables();
        evaluator.run(false);
        d->control_node->setOutputs();
    }
}

dtkComposerGraphNodeList dtkComposerGraphNodeBeginForEach::successors(void)
{
    dtkComposerGraphNodeList list; list << dtkComposerGraphNodeBegin::end();

    return list;
}

//
// dtkComposerGraphNodeBeginForEach.cpp ends here
