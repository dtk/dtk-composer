/* dtkComposerTagCloudDesc.h ---
 *
 * Author: Julien Wintz
 * Created: Mon Apr 15 14:36:15 2013 (+0200)
 */

/* Change Log:
 *
 */

#pragma once

#include <dtkComposerExport.h>

#include <QtWidgets>

class DTKCOMPOSER_EXPORT dtkComposerTagCloudDescPrivate;

class dtkComposerTagCloudDesc : public QFrame
{
    Q_OBJECT

public:
    dtkComposerTagCloudDesc(QWidget *parent = 0);
    ~dtkComposerTagCloudDesc(void);

signals:
    void back(void);

public slots:
    void clear(void);

public slots:
    void setDescription(const QString& description);

protected:
    dtkComposerTagCloudDescPrivate *d;
};
