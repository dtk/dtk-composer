// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeGraphBase interface
// /////////////////////////////////////////////////////////////////

template <typename Node, typename Edge>
class dtkComposerNodeGraphBase
{
};

template <typename Node, typename Edge>
class dtkComposerNodeGraphBase<Node *, Edge>
{
public:
             dtkComposerNodeGraphBase(void) = default;
             dtkComposerNodeGraphBase(const dtkComposerNodeGraphBase& o);
    virtual ~dtkComposerNodeGraphBase(void);

public:
    dtkComposerNodeGraphBase& operator = (const dtkComposerNodeGraphBase& o);

public:
    QString description(void) const;

public:
    void clear(void);

public:
    bool contains(Node *n) const;
    bool contains(const Edge& e) const;

public:
    virtual void addNode(Node *n);
    virtual void addEdge(const Edge& e);
    virtual void removeNode(Node *n);
    virtual void removeEdge(const Edge& e);

public:
    QList<Node *> rootNodes(void) const;
    QList<Node *> succcessors(Node *n) const;
    QList<Node *> predecessors(Node *n) const;

public:
    const QList<Node *>& nodes(void) const;
    const QList<Edge  >& edges(void) const;

public:
    QList<Node *> topologicalSort(void) const;

public:
    dtkComposerNodeGraphBase subgraph(Node *from, Node *to) const;

protected:
    QList<Node *> m_nodes;
    QList<Edge  > m_edges;

    QHash<Node *, QList<Node *>> m_predecessors;
    QHash<Node *, QList<Node *>> m_successors;
};

// /////////////////////////////////////////////////////////////////

#include "dtkComposerNodeGraphBase.tpp"

//
// dtkComposerNodeGraphBase.h ends here
