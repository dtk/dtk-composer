// Version: $Id: f0bfbda5344a2778cc093b15c0e123b779b6fc5b $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport.h>

#include <QtWidgets>

class dtkComposerTagCloudItem;
class dtkComposerTagCloudListPrivate;

class DTKCOMPOSER_EXPORT dtkComposerTagCloudList : public QListWidget
{
    Q_OBJECT

public:
    dtkComposerTagCloudList(QWidget *parent = 0);
    ~dtkComposerTagCloudList(void);

    void addItem(QString name);
    void addItem(QString name, QString description);
    void addItem(QString name, QString description, QStringList tags);
    void addItem(QString name, QString description, QStringList tags, QString kind, QString type);
    void addItem(dtkComposerTagCloudItem item);

    void clear(void);

public:
    void setDark(void);
    void setDoom(void);

signals:
    void itemClicked(const QString& description);

protected slots:
    void onItemClicked(QListWidgetItem *item);

protected:
    QMimeData *mimeData(const QList<QListWidgetItem *> items) const;
    QStringList mimeTypes(void) const;

protected:
    dtkComposerTagCloudListPrivate *d;

protected:
    friend class dtkComposerTagCloudListLightDelegate;
    friend class dtkComposerTagCloudListDarkDelegate;
    friend class dtkComposerTagCloudListDoomDelegate;
};

//
// dtkComposerTagCloudList.h ends here
