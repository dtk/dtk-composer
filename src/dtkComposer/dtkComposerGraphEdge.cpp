// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerGraphEdge.h"

class dtkComposerGraphEdgePrivate
{
public:
    int id = 0;
    dtkComposerGraphNode *source = nullptr;
    dtkComposerGraphNode *destination = nullptr;
};

// ///////////////////////////////////////////////////////////////////

dtkComposerGraphEdge::dtkComposerGraphEdge(void) : d(new dtkComposerGraphEdgePrivate)
{
}

dtkComposerGraphEdge::dtkComposerGraphEdge(dtkComposerGraphNode *source, dtkComposerGraphNode *destination) : d(new dtkComposerGraphEdgePrivate)
{
    d->source      = source;
    d->destination = destination;
}

dtkComposerGraphEdge::dtkComposerGraphEdge(const dtkComposerGraphEdge& other) : d(new dtkComposerGraphEdgePrivate)
{
    d->source      = other.d->source;
    d->destination = other.d->destination;
}

dtkComposerGraphEdge::~dtkComposerGraphEdge(void)
{
    delete d;
}

dtkComposerGraphEdge& dtkComposerGraphEdge::operator = (const dtkComposerGraphEdge& other)
{
    d->source      = other.d->source;
    d->destination = other.d->destination;

    return (*this);
}

bool dtkComposerGraphEdge::operator == (const dtkComposerGraphEdge& other) const
{
    return (d->source == other.d->source) && (d->destination == other.d->destination);
}

dtkComposerGraphNode *dtkComposerGraphEdge::source(void) const
{
    return d->source;
}

dtkComposerGraphNode *dtkComposerGraphEdge::destination(void) const
{
    return d->destination;
}

void dtkComposerGraphEdge::setSource(dtkComposerGraphNode *source)
{
    d->source = source;
}

void dtkComposerGraphEdge::setDestination(dtkComposerGraphNode *destination)
{
    d->destination = destination;
}

int dtkComposerGraphEdge::id(void) const
{
    return d->id;
}

void dtkComposerGraphEdge::setId(int id)
{
    d->id = id;
}

//
// dtkComposerGraphEdge.cpp ends here
