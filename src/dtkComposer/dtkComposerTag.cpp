// Version: $Id: 4e5833fc9193b19e660e964a4a47af48ab84cd0f $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerTag.h"

class dtkComposerTagPrivate
{
public:
    int count;
    QString text;
    QString color;
    QStringList items;
};

dtkComposerTag::dtkComposerTag(void) : d(new dtkComposerTagPrivate)
{

}

dtkComposerTag::dtkComposerTag(QString text, int count) : d(new dtkComposerTagPrivate)
{
    d->text = text;
    d->count = count;
}

dtkComposerTag::dtkComposerTag(QString text, int count, QStringList items) : d(new dtkComposerTagPrivate)
{
    d->text = text;
    d->count = count;
    d->items = items;
}

dtkComposerTag::dtkComposerTag(QString text, int count, QStringList items, QString color) : d(new dtkComposerTagPrivate)
{
    d->text = text;
    d->count = count;
    d->items = items;
    d->color = color;
}

dtkComposerTag::dtkComposerTag(const dtkComposerTag& other) : d(new dtkComposerTagPrivate)
{
    d->text = other.d->text;
    d->count = other.d->count;
    d->items = other.d->items;
    d->color = other.d->color;
}

dtkComposerTag::~dtkComposerTag(void)
{
    delete d;

    d = NULL;
}

int dtkComposerTag::count(void) const
{
    return d->count;
}

QString dtkComposerTag::text(void) const
{
    return d->text;
}

QStringList dtkComposerTag::items(void) const
{
    return d->items;
}

QString dtkComposerTag::color(void) const
{
    return d->color;
}

void dtkComposerTag::setCount(int count)
{
    if (count > 0)
        d->count = count;
    else
        d->count = count;
}

void dtkComposerTag::setText(QString text)
{
    if (!text.isNull())
        d->text = text;
    else
        d->text = "";
}

void dtkComposerTag::setItems(QStringList items)
{
    if (!items.isEmpty())
        d->items = items;
    else
        d->items = QStringList();
}

void dtkComposerTag::setColor(QString color)
{
    if (!color.isNull())
        d->color = color;
    else
        d->color = "";
}

//
// dtkComposerTag.cpp ends here
