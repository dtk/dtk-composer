/* @(#)dtkComposerGraphNodeEnd.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) 2012 - Nicolas Niclausse, Inria.
 * Created: 2012/02/14 13:59:57
 * Version: $Id: 0eb86ede1bf72d2af2ff28396ac02c0907199373 $
 * Last-Updated: Thu Apr 11 10:25:30 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 192
 */

/* Commentary:
 *
 */

/* Change log:
 *
 */

#include "dtkComposerGraphNodeEnd.h"

#include <dtkComposerConfig.h>

#include "dtkComposerGraphNodeBegin.h"
#include "dtkComposerNodeControl.h"
#include "dtkComposerNodeComposite.h"

#include <dtkLog>

class dtkComposerGraphNodeEndPrivate
{
public:
    dtkComposerNodeControl *control_node;
    dtkComposerNodeComposite *composite;

    bool is_remote;

    dtkComposerGraphNodeBegin *begin;
};


dtkComposerGraphNodeEnd::dtkComposerGraphNodeEnd(dtkComposerNode *cnode, const QString& title) : dtkComposerGraphNode(), d(new dtkComposerGraphNodeEndPrivate)
{
    d->is_remote = false;

    if (!dynamic_cast<dtkComposerNodeControl *>(cnode)) {
        d->composite = dynamic_cast<dtkComposerNodeComposite *>(cnode);
        d->control_node = nullptr;

    } else {
        d->control_node = dynamic_cast<dtkComposerNodeControl *>(cnode);
        d->composite = nullptr;
    }

    d->begin = nullptr;
    this->setTitle(title);
}

dtkComposerGraphNode::Kind dtkComposerGraphNodeEnd::kind(void)
{
    return dtkComposerGraphNode::End;
}

dtkComposerNode *dtkComposerGraphNodeEnd::wrapee(void)
{
    if (!d->control_node)
        return d->composite;
    else
        return d->control_node;
}

void dtkComposerGraphNodeEnd::eval(void)
{
    if (!d->control_node) { // composite node end
        if (d->composite)// may be null for root node
            d->composite->end();
    } else
        d->control_node->end();

    this->setStatus(dtkComposerGraphNode::Done);
}

void dtkComposerGraphNodeEnd::setBegin(dtkComposerGraphNodeBegin *begin)
{
    d->begin = begin;
}

dtkComposerGraphNodeList dtkComposerGraphNodeEnd::predecessors(void)
{
    return dtkComposerGraphNode::predecessors();
}
