// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeGraphEdgeBase interface
// /////////////////////////////////////////////////////////////////

template <typename Node>
class dtkComposerNodeGraphEdgeBase
{
public:
             dtkComposerNodeGraphEdgeBase(void) = default;
             dtkComposerNodeGraphEdgeBase(const Node& source, const Node& destination);
             dtkComposerNodeGraphEdgeBase(const dtkComposerNodeGraphEdgeBase& o);
    virtual ~dtkComposerNodeGraphEdgeBase(void) = default;

public:
    dtkComposerNodeGraphEdgeBase& operator = (const dtkComposerNodeGraphEdgeBase& o);

public:
    bool operator == (const dtkComposerNodeGraphEdgeBase& o) const;

public:
    virtual const Node& source(void) const;
    virtual const Node& destination(void) const;

public:
    void setSource(const Node& source);
    void setDestination(const Node& destination);

protected:
    Node m_source;
    Node m_destination;
};

// /////////////////////////////////////////////////////////////////

#include "dtkComposerNodeGraphEdgeBase.tpp"

//
// dtkComposerNodeGraphEdgeBase.h ends here
