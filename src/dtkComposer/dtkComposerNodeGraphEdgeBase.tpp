// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

// /////////////////////////////////////////////////////////////////
// dtkComposerNodeGraphEdgeBase implementation
// /////////////////////////////////////////////////////////////////

template <typename Node>
inline dtkComposerNodeGraphEdgeBase<Node>::dtkComposerNodeGraphEdgeBase(const Node& source, const Node& destination) : m_source(source),
                                                                                                                       m_destination(destination)
{
}

template <typename Node>
inline dtkComposerNodeGraphEdgeBase<Node>::dtkComposerNodeGraphEdgeBase(const dtkComposerNodeGraphEdgeBase& o) : m_source(o.m_source),
                                                                                                                 m_destination(o.m_destination)
{
}

template <typename Node>
inline dtkComposerNodeGraphEdgeBase<Node>& dtkComposerNodeGraphEdgeBase<Node>::operator = (const dtkComposerNodeGraphEdgeBase& o)
{
    m_source = o.m_source;
    m_destination = o.m_destination;

    return *this;
}

template <typename Node>
inline bool dtkComposerNodeGraphEdgeBase<Node>::operator == (const dtkComposerNodeGraphEdgeBase& o) const
{
    return (m_source == o.m_source && m_destination == o.m_destination);
}

template <typename Node>
inline const Node& dtkComposerNodeGraphEdgeBase<Node>::source(void) const
{
    return m_source;
}

template <typename Node>
inline const Node& dtkComposerNodeGraphEdgeBase<Node>::destination(void) const
{
    return m_destination;
}

template <typename Node>
inline void dtkComposerNodeGraphEdgeBase<Node>::setSource(const Node& source)
{
    m_source = source;
}

template <typename Node>
inline void dtkComposerNodeGraphEdgeBase<Node>::setDestination(const Node& destination)
{
    m_destination = destination;
}

//
// dtkComposerNodeGraphEdgeBase.tpp ends here
