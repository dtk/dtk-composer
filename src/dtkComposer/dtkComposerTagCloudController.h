// Version: $Id: f66553d2437e42e70156fde3bab0a38a4308e038 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport.h>

#include <QtCore>

class dtkComposerTagCloud;
class dtkComposerTagCloudView;
class dtkComposerTagCloudScope;
class dtkComposerTagCloudControllerPrivate;

class DTKCOMPOSER_EXPORT dtkComposerTagCloudController : public QObject
{
    Q_OBJECT

public:
    dtkComposerTagCloudController(void);
    ~dtkComposerTagCloudController(void);

    void attach(dtkComposerTagCloud *cloud);
    void attach(dtkComposerTagCloudView *view);
    void attach(dtkComposerTagCloudScope *scope);

    void addItem(QString name);
    void addItem(QString name, QString description);
    void addItem(QString name, QString description, QStringList tags);
    void addItem(QString name, QString description, QStringList tags, QString kind, QString type);

    void update(void);
    void render(void);

public slots:
    void onUnionMode(bool mode);

protected slots:
    void addFilter(QString tag);
    void setFilter(QString tag);
    void remFilter(QString tag);
    void clear(void);

protected:
    dtkComposerTagCloudControllerPrivate *d;
};

//
// dtkComposerTagCloudController.h ends here
