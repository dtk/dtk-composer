// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport.h>

#include "dtkComposerTag.h"

#include <QtWidgets>

class dtkComposerTagCloudPrivate;

class DTKCOMPOSER_EXPORT dtkComposerTagCloud : public QTextBrowser
{
    Q_OBJECT

public:
    enum SortingType {
        Alpha,
        Num
    };

    enum SortingOrder {
        Asc,
        Desc
    };

public:
    dtkComposerTagCloud(QWidget *parent = 0);
    ~dtkComposerTagCloud(void);

#pragma mark -
#pragma mark Tag management

    void addTag(QString text, int instances);
    void addTag(QString text, int instances, QStringList items);
    void addTag(QString text, int instances, QStringList items, QString color);
    void addTag(dtkComposerTag tag);

#pragma mark -
#pragma mark Font management

    void setFontSize(int size);
    void setFontRange(int range);

#pragma mark -
#pragma mark Sorting

    void setSortingType(SortingType type);
    void setSortingOrder(SortingOrder order);

signals:
    void tagClicked(QString tag);
    void tagClicked(QString tag, QStringList items);

public slots:
    void sort(void);
    void clear(void);
    void render(void);

protected slots:
    void onLinkClicked(const QUrl& item);

protected:
    dtkComposerTagCloudPrivate *d;
};

//
// dtkComposerTagCloud.h ends here
