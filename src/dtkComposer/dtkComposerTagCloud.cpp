/* dtkComposerTagCloud.cpp ---
 *
 * Author: Julien Wintz
 * Created: Mon Apr 15 12:00:34 2013 (+0200)
 * Version:
 * Last-Updated: mar. avril  8 14:54:47 2014 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 33
 */

/* Change Log:
 *
 */

#include "dtkComposerTag.h"
#include "dtkComposerTagCloud.h"

// /////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////

bool dtkComposerTagAlphaLessThan(const dtkComposerTag& t1, const dtkComposerTag& t2)
{
    return t1.text() < t2.text();
}

bool dtkComposerTagNumLessThan(const dtkComposerTag& t1, const dtkComposerTag& t2)
{
    return t1.count() < t2.count();
}

bool dtkComposerTagAlphaMoreThan(const dtkComposerTag& t1, const dtkComposerTag& t2)
{
    return t1.text() >= t2.text();
}

bool dtkComposerTagNumMoreThan(const dtkComposerTag& t1, const dtkComposerTag& t2)
{
    return t1.count() >= t2.count();
}

// /////////////////////////////////////////////////////////////////
// dtkComposerTagCloudHasher
// /////////////////////////////////////////////////////////////////

class dtkComposerTagCloudHasher
{
public:
    dtkComposerTagCloudHasher(int buckets, int min, int max) {
        if (buckets < 1)
            qDebug() << "dtkComposerTagCloudHasher: Must have at least one bucket.";

        this->buckets = buckets;
        this->min = min;
        this->max = max + 1;

        this->width = ((double)(this->max - this->min)) / ((double)(this->buckets));
    }

    int bucket(dtkComposerTag tag) {
        return ((float)(tag.count() - this->min)) / ((float)(this->width));
    }

private:
    int min, max, buckets;

private:
    double width;
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerTagCloudPrivate
// ///////////////////////////////////////////////////////////////////

class dtkComposerTagCloudPrivate
{
public:
    QList<dtkComposerTag> tags;

    int averageFontSize;
    int fontSizeRange;
    int mincount;
    int maxcount;
    int tagCount;

    dtkComposerTagCloud::SortingType  sortingType;
    dtkComposerTagCloud::SortingOrder sortingOrder;
};

// /////////////////////////////////////////////////////////////////
// dtkComposerTagCloud
// /////////////////////////////////////////////////////////////////

/*!
  \class dtkComposerTagCloud

  \inmodule dtkWidgets

  \brief The dtkComposerTagCloud class provides a configurable tag
  cloud.

  \l {Core Module} {Core Module} ...
*/

dtkComposerTagCloud::dtkComposerTagCloud(QWidget *parent) : QTextBrowser(parent)
{
    d = new dtkComposerTagCloudPrivate;

    d->averageFontSize = 0;
    d->fontSizeRange   = 0;

    d->sortingType  = Alpha;
    d->sortingOrder = Asc;

    this->setFrameShape(QFrame::NoFrame);

    connect(this, SIGNAL(anchorClicked(const QUrl&)), this, SLOT(onLinkClicked(const QUrl&)));
}

dtkComposerTagCloud::~dtkComposerTagCloud(void)
{
    delete d;

    d = NULL;
}

void dtkComposerTagCloud::addTag(QString text, int count)
{
    d->tags << dtkComposerTag(text, count);
}

void dtkComposerTagCloud::addTag(QString text, int count, QStringList items)
{
    d->tags << dtkComposerTag(text, count, items);
}

void dtkComposerTagCloud::addTag(QString text, int count, QStringList items, QString color)
{
    d->tags << dtkComposerTag(text, count, items, color);
}

void dtkComposerTagCloud::addTag(dtkComposerTag tag)
{
    d->tags << tag;
}

void dtkComposerTagCloud::setFontSize(int size)
{
    d->averageFontSize = (size > 0) ? size : 0;
}

void dtkComposerTagCloud::setFontRange(int range)
{
    d->fontSizeRange = (range > 0) ? range : 0;
}

void dtkComposerTagCloud::setSortingType(SortingType type)
{
    d->sortingType = type;
}

void dtkComposerTagCloud::setSortingOrder(SortingOrder order)
{
    d->sortingOrder = order;
}

void dtkComposerTagCloud::sort(void)
{
    if (d->sortingType == Alpha && d->sortingOrder == Asc)
        qSort(d->tags.begin(), d->tags.end(), dtkComposerTagAlphaLessThan);

    if (d->sortingType == Alpha && d->sortingOrder == Desc)
        qSort(d->tags.begin(), d->tags.end(), dtkComposerTagAlphaMoreThan);

    if (d->sortingType == Num && d->sortingOrder == Asc)
        qSort(d->tags.begin(), d->tags.end(), dtkComposerTagNumLessThan);

    if (d->sortingType == Num && d->sortingOrder == Desc)
        qSort(d->tags.begin(), d->tags.end(), dtkComposerTagNumMoreThan);
}

void dtkComposerTagCloud::clear(void)
{
    d->tags.clear();

    this->render();
}

void dtkComposerTagCloud::render(void)
{
    if (d->averageFontSize <= 0)
        qDebug() << "AverageFontSize must be non-zero and non-negative.";

    if (d->fontSizeRange <= 0)
        qDebug() << "FontSizeRange must be non-zero and non-negative.";

    if (d->tags.isEmpty())
        return;

    d->mincount = 100000000;
    d->maxcount = 0;
    d->tagCount = 0;

    for (dtkComposerTag tag : d->tags) {

        if (tag.count() < d->mincount)
            d->mincount = tag.count();

        if (tag.count() > d->maxcount)
            d->maxcount = tag.count();

        d->tagCount++;
    }

    dtkComposerTagCloudHasher hasher(d->fontSizeRange, d->mincount, d->maxcount);

    int baseFontSize = d->averageFontSize - ((double)(d->fontSizeRange - 1) / 2);

    QString cloud; cloud.append(QString("<div align=\"justify\">\n"));

    for (dtkComposerTag tag : d->tags) {

        int fontSize = baseFontSize + hasher.bucket(tag);

        QString color = "";

        if (!tag.color().isEmpty()) {
            color  = "color: ";
            color += (!tag.color().startsWith("#")) ? "#" : "";
            color += tag.color();
            color += ";";
        } else {
            color = "color: #6a769d;";
        }

        QString count = QString::number(tag.count()) + " item" + ((tag.count() != 1) ? "s" : "");

        cloud.append(QString("<a href=\"tag://%1\" title=\"%2\" style=\"font-size: %4px; text-decoration: none; %5\" item=\"%3\">%1</a> ")
                     .arg(tag.text())
                     .arg(count)
                     .arg(tag.items().first())
                     .arg(fontSize)
                     .arg(color));
    }

    cloud.append("</div>\n");

    this->setHtml(cloud);
}

void dtkComposerTagCloud::onLinkClicked(const QUrl& url)
{
    emit tagClicked(url.host());
}
