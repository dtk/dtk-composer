// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport>

#include "dtkComposerGraphTemplate.h"
#include "dtkComposerGraphNode.h"
#include "dtkComposerGraphEdge.h"

#include <QtCore>

class dtkComposerNode;

class DTKCOMPOSER_EXPORT dtkComposerGraph : public QObject
{
    Q_OBJECT

public:
    using Node = dtkComposerGraphNode;
    using Edge = dtkComposerGraphEdge;
    using Graph = dtkComposerGraphTemplate<Node *, Edge>;


     dtkComposerGraph(void);
    ~dtkComposerGraph(void);

    class dtkComposerGraphNode *root(void);

    void    addNode(dtkComposerNode *node, dtkComposerNode *parent);
    void removeNode(dtkComposerNode *node, dtkComposerNode *parent);

    void    addBlock(dtkComposerNode *node);
    void removeBlock(dtkComposerNode *node, dtkComposerNode *parent);

    void    addEdge(dtkComposerNode *source, dtkComposerNode *destination, QString src_type, QString dst_type);
    void removeEdge(dtkComposerNode *source, dtkComposerNode *destination, QString src_type, QString dst_type, dtkComposerNode *parent);

    void reparentNode(dtkComposerNode *node, dtkComposerNode *oldparent, dtkComposerNode *newparent);

    void createGroup(dtkComposerNode *node, dtkComposerNode *parent);
    void destroyGroup(dtkComposerNode *node, dtkComposerNode *parent);
    void removeGroup(dtkComposerNode *node, dtkComposerNode *parent);

    dtkComposerGraphEdgeList edges(void);
    dtkComposerGraphNodeList nodes(void);

    Graph graph(void);
    Graph subgraph(dtkComposerGraphNode *from, dtkComposerGraphNode *to);

    void removeNode(      dtkComposerGraphNode *node);
    void removeEdge(class dtkComposerGraphEdge *edge);

    void clear(void);

    QString toString(void);

signals:
    void cleared(void);

private:
    class dtkComposerGraphPrivate *d;
};

//
// dtkComposerGraph.h ends here
