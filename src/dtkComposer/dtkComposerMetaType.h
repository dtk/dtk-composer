// Version: $Id: dcbbe165c9cb30a40224eeb92ce68f25677d1380 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>
#include <QtGui>

Q_DECLARE_METATYPE(QImage *);
Q_DECLARE_METATYPE(QByteArray *);

//
// dtkComposerMetaType.h ends here
