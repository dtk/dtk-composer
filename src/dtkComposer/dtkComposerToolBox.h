// Version: $Id: 8dd4802930facec66d78da59defe3a36ce77e037 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport.h>

#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKCOMPOSER_EXPORT dtkComposerToolBoxButton : public QAbstractButton
{
    Q_OBJECT

public:
    dtkComposerToolBoxButton(QWidget *parent);

public:
    QSize sizeHint(void) const;
    QSize minimumSizeHint(void) const;

public:
    void setSelected(bool selected);
    bool isSelected(void) const;

protected:
    void initStyleOption(QStyleOptionToolBox *opt) const;
    void paintEvent(QPaintEvent *event);

private:
    bool m_selected;
};

inline void dtkComposerToolBoxButton::setSelected(bool selected)
{
    m_selected = selected;
}

inline bool dtkComposerToolBoxButton::isSelected(void) const
{
    return m_selected;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkComposerToolBox;
class dtkComposerToolBoxItemPrivate;

class DTKCOMPOSER_EXPORT dtkComposerToolBoxItem : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(bool expanded READ isExpanded WRITE setExpanded)
    Q_PROPERTY(bool enforced READ isEnforced WRITE setEnforced)
    Q_PROPERTY(QString  name READ       name WRITE     setName)

public:
    explicit dtkComposerToolBoxItem(QWidget *parent = 0);
    ~dtkComposerToolBoxItem(void);

public:
    bool isExpanded(void) const;
    bool isEnforced(void) const;
    QString    name(void) const;

public:
    void showButton(void);
    void hideButton(void);

public:
    void setWidget(QWidget *widget, const QString& text, const QIcon& icon = QIcon());

public slots:
    void setExpanded(bool expanded);
    void setEnforced(bool enforced);
    void setName(const QString& name);

public slots:
    void onButtonClicked(void);

public:
    static dtkComposerToolBoxItem *fromObject(QObject *object, int hierarchy_level = -1);

public:
    void setToolBox(dtkComposerToolBox *box);

private:
    dtkComposerToolBoxItemPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkComposerToolBoxPrivate;

class DTKCOMPOSER_EXPORT dtkComposerToolBox : public QScrollArea
{
    Q_OBJECT
    Q_PROPERTY(int count READ count)
    Q_PROPERTY(Order order READ order WRITE setOrder)
    Q_ENUMS(Order)
    Q_PROPERTY(DisplayMode displayMode READ displayMode WRITE setDisplayMode)
    Q_ENUMS(DisplayMode)

public:
    enum Order {
        Ascending,
        Descending,
        AlphaBetics
    };

public:
    enum DisplayMode {
        Default,
        AllItemExpanded,
        OneItemExpanded
    };

public:
    explicit dtkComposerToolBox(QWidget *parent = 0);
    ~dtkComposerToolBox(void);

public:
    void clear(void);

public:
    int count(void) const;
    Order order(void) const;
    DisplayMode displayMode(void) const;

public:
    dtkComposerToolBoxItem *itemAt(int index) const;

public:
    void    addItem(dtkComposerToolBoxItem *item);
    void insertItem(int index, dtkComposerToolBoxItem *item);

public:
    void setCurrentItem(dtkComposerToolBoxItem *item);

public:
    void removeItem(int index);
    void removeItem(dtkComposerToolBoxItem *item);

public slots:
    void setOrder(Order order);
    void setDisplayMode(DisplayMode mode);

private:
    dtkComposerToolBoxPrivate *d;
};

inline void dtkComposerToolBox::addItem(dtkComposerToolBoxItem *item)
{
    this->insertItem(this->count(), item);
}

//
// dtkComposerToolBox.h ends here
