// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkComposerToolBox.h"

class dtkComposerSceneNode;

class dtkComposerControlsListItem : public dtkComposerToolBoxItem
{
    Q_OBJECT

public:
    dtkComposerControlsListItem(QWidget *parent = 0, dtkComposerSceneNode *node = NULL);
    virtual ~dtkComposerControlsListItem(void);
};

//
// dtkComposerControlsListItem.h ends here
