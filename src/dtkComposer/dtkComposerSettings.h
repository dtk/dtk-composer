// Version: $Id: d1d9775ad73f50a84f8ce91d960a4ab7e51eda13 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport.h>

#include <QtCore>

class DTKCOMPOSER_EXPORT dtkComposerSettings : public QSettings
{
public:
    dtkComposerSettings(void);
    ~dtkComposerSettings(void);
};

//
// dtkComposerSettings.h ends here
