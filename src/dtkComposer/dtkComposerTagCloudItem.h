// Version: $Id: 416c3881f3ee861a0529ccb6ed31c568b46f9331 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport.h>
#include <QtWidgets>

class dtkComposerTagCloudItemPrivate;

class DTKCOMPOSER_EXPORT dtkComposerTagCloudItem : public QListWidgetItem
{
public:
     dtkComposerTagCloudItem(QString name);
     dtkComposerTagCloudItem(QString name, QString description);
     dtkComposerTagCloudItem(QString name, QString description, QStringList tags);
     dtkComposerTagCloudItem(QString name, QString description, QStringList tags, QString kind, QString type);
     dtkComposerTagCloudItem(const dtkComposerTagCloudItem& item);
    ~dtkComposerTagCloudItem(void);

public:
    QString name(void) const;
    QString description(void) const;
    QStringList tags(void) const;
    QString kind(void) const;
    QString type(void) const;

protected:
    dtkComposerTagCloudItemPrivate *d;
};

//
// dtkComposerTagCloudItem.h ends here
