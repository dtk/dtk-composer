// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerToolBox.h"

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditorPrivate
{
public:
    QObject *object;
    QMetaProperty meta_property;
    QWidget *editor;
    QLayout *layout;
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditor declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditor : public QFrame
{
    Q_OBJECT

public:
             dtkComposerPropertyEditor(const QString& property_name, QObject *object, QWidget *parent = 0);
    virtual ~dtkComposerPropertyEditor(void);

public:
    void  setValue(const QVariant& value);
    QVariant value(void) const;

public:
    QObject *propertyObject(void);

public:
    virtual void  setEditorData(const QVariant& data) = 0;
    virtual QVariant editorData(void) = 0;

protected:
    dtkComposerPropertyEditorPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorDouble declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditorDouble : public dtkComposerPropertyEditor
{
    Q_OBJECT

public:
    dtkComposerPropertyEditorDouble(const QString& property_name, QObject *object, QWidget *parent = 0);
    ~dtkComposerPropertyEditorDouble(void);

public:
    void  setEditorData(const QVariant& data);
    QVariant editorData(void);

public slots:
    void onValueChanged(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorInteger declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditorInteger : public dtkComposerPropertyEditor
{
    Q_OBJECT

public:
     dtkComposerPropertyEditorInteger(const QString& property_name, QObject *object, QWidget *parent = 0);
    ~dtkComposerPropertyEditorInteger(void);

public:
    void  setEditorData(const QVariant& data);
    QVariant editorData(void);

public slots:
    void onValueChanged(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorString declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditorString : public dtkComposerPropertyEditor
{
    Q_OBJECT

public:
     dtkComposerPropertyEditorString(const QString& property_name, QObject *object, QWidget *parent = 0);
    ~dtkComposerPropertyEditorString(void);

public:
    void  setEditorData(const QVariant& data);
    QVariant editorData(void);

public slots:
    void onTextChanged(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorEnum declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditorEnum : public dtkComposerPropertyEditor
{
    Q_OBJECT

public:
     dtkComposerPropertyEditorEnum(const QString& property_name, QObject *object, QWidget *parent = 0);
    ~dtkComposerPropertyEditorEnum(void);

public:
    void  setEditorData(const QVariant& data);
    QVariant editorData(void);

public slots:
    void onIndexChanged(int index);
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditor implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerPropertyEditor::dtkComposerPropertyEditor(const QString& property_name, QObject *object, QWidget *parent) : QFrame(parent), d(new dtkComposerPropertyEditorPrivate)
{
    if (!object) {
        qDebug() << Q_FUNC_INFO << "dtkComposerPropertyEditor must be created using a valid QObject.";
        return;
    }

    d->object = object;
    this->setObjectName(property_name);

    d->meta_property = object->metaObject()->property(object->metaObject()->indexOfProperty(qPrintable(property_name)));

    d->editor = NULL;
    d->layout = NULL;
}

dtkComposerPropertyEditor::~dtkComposerPropertyEditor(void)
{
    if (d->editor)
        delete d->editor;

    delete d;

    d = NULL;
}

void dtkComposerPropertyEditor::setValue(const QVariant& value)
{
    d->object->setProperty(d->meta_property.name(), value);
}

QVariant dtkComposerPropertyEditor::value(void) const
{
    return d->object->property(d->meta_property.name());
}

QObject *dtkComposerPropertyEditor::propertyObject(void)
{
    return d->object;
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorDouble implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerPropertyEditorDouble::dtkComposerPropertyEditorDouble(const QString& property_name, QObject *object, QWidget *parent) : dtkComposerPropertyEditor(property_name, object, parent)
{
    QDoubleSpinBox *spin_d = new QDoubleSpinBox(this);
    spin_d->setObjectName(property_name);
    spin_d->setMinimum(-999999999);
    spin_d->setMaximum(+999999999);
    spin_d->setDecimals(14);
    spin_d->setSingleStep(1);
    spin_d->setEnabled(true);
    spin_d->setValue(object->property(d->meta_property.name()).toDouble());

    QObject::connect(spin_d, SIGNAL(editingFinished()), this, SLOT(onValueChanged()));

    d->editor = spin_d;

    d->layout = new QHBoxLayout;
    d->layout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(d->layout);
    d->layout->addWidget(d->editor);
}

dtkComposerPropertyEditorDouble::~dtkComposerPropertyEditorDouble(void)
{

}

void dtkComposerPropertyEditorDouble::setEditorData(const QVariant& data)
{
    d->editor->blockSignals(true);
    static_cast<QDoubleSpinBox *>(d->editor)->setValue(data.toDouble());
    d->editor->blockSignals(false);
}

QVariant dtkComposerPropertyEditorDouble::editorData(void)
{
    return QVariant::fromValue(static_cast<QDoubleSpinBox *>(d->editor)->value());
}

void dtkComposerPropertyEditorDouble::onValueChanged(void)
{
    d->object->setProperty(d->meta_property.name(), this->editorData());
}

dtkComposerPropertyEditor *createDtkPropertyEditorDouble(const QString& property_name, QObject *object, QWidget *parent)
{
    return new dtkComposerPropertyEditorDouble(property_name, object, parent);
}


// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorInteger implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerPropertyEditorInteger::dtkComposerPropertyEditorInteger(const QString& property_name, QObject *object, QWidget *parent) : dtkComposerPropertyEditor(property_name, object, parent)
{
    QSpinBox *spin_i = new QSpinBox(this);
    spin_i->setObjectName(property_name);
    spin_i->setMinimum(-999999999);
    spin_i->setMaximum(+999999999);
    spin_i->setSingleStep(1);
    spin_i->setEnabled(true);
    spin_i->setValue(object->property(d->meta_property.name()).toLongLong());

    QObject::connect(spin_i, SIGNAL(editingFinished()), this, SLOT(onValueChanged()));

    d->editor = spin_i;

    d->layout = new QHBoxLayout;
    d->layout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(d->layout);
    d->layout->addWidget(d->editor);
}

dtkComposerPropertyEditorInteger::~dtkComposerPropertyEditorInteger(void)
{

}

void dtkComposerPropertyEditorInteger::setEditorData(const QVariant& data)
{
    d->editor->blockSignals(true);
    static_cast<QSpinBox *>(d->editor)->setValue(data.toLongLong());
    d->editor->blockSignals(false);
}

QVariant dtkComposerPropertyEditorInteger::editorData(void)
{
    return QVariant::fromValue(static_cast<QSpinBox *>(d->editor)->value());
}

void dtkComposerPropertyEditorInteger::onValueChanged(void)
{
    d->object->setProperty(d->meta_property.name(), this->editorData());
}

dtkComposerPropertyEditor *createDtkPropertyEditorInteger(const QString& property_name, QObject *object, QWidget *parent)
{
    return new dtkComposerPropertyEditorInteger(property_name, object, parent);
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorString implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerPropertyEditorString::dtkComposerPropertyEditorString(const QString& property_name, QObject *object, QWidget *parent) : dtkComposerPropertyEditor(property_name, object, parent)
{
    QLineEdit *edit = new QLineEdit(this);
    edit->setObjectName(property_name);
    edit->setEnabled(true);
    edit->setText(object->property(d->meta_property.name()).toString());

    QObject::connect(edit, SIGNAL(returnPressed()), this, SLOT(onTextChanged()));

    d->editor = edit;

    d->layout = new QHBoxLayout;
    d->layout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(d->layout);
    d->layout->addWidget(d->editor);
}

dtkComposerPropertyEditorString::~dtkComposerPropertyEditorString(void)
{

}

void dtkComposerPropertyEditorString::setEditorData(const QVariant& data)
{
    d->editor->blockSignals(true);
    static_cast<QLineEdit *>(d->editor)->setText(data.toString());
    d->editor->blockSignals(false);
}

QVariant dtkComposerPropertyEditorString::editorData(void)
{
    return QVariant::fromValue(static_cast<QLineEdit *>(d->editor)->text());
}

void dtkComposerPropertyEditorString::onTextChanged(void)
{
    d->object->setProperty(d->meta_property.name(), this->editorData());
}

dtkComposerPropertyEditor *createDtkPropertyEditorString(const QString& property_name, QObject *object, QWidget *parent)
{
    return new dtkComposerPropertyEditorString(property_name, object, parent);
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorEnum implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerPropertyEditorEnum::dtkComposerPropertyEditorEnum(const QString& property_name, QObject *object, QWidget *parent) : dtkComposerPropertyEditor(property_name, object, parent)
{
    QComboBox *list = new QComboBox(parent);
    list->setObjectName(property_name);
    list->setEnabled(true);

    int current_value = object->property(d->meta_property.name()).toInt();

    QMetaEnum meta_enum = d->meta_property.enumerator();
    int count = meta_enum.keyCount();
    int current_index = 0;

    for (int i = 0; i < count; ++i) {
        list->addItem(meta_enum.key(i));

        if (meta_enum.value(i) == current_value)
            current_index = i;
    }

    list->setCurrentIndex(current_index);

    QObject::connect(list, SIGNAL(currentIndexChanged(int)), this, SLOT(onIndexChanged(int)));

    d->editor = list;

    d->layout = new QHBoxLayout;
    d->layout->setContentsMargins(0, 0, 0, 0);
    this->setLayout(d->layout);
    d->layout->addWidget(d->editor);
}

dtkComposerPropertyEditorEnum::~dtkComposerPropertyEditorEnum(void)
{

}

void dtkComposerPropertyEditorEnum::setEditorData(const QVariant& data)
{
    int current_index = -1;

    switch (data.type()) {

    case QVariant::Int:
        current_index = data.toInt();
        break;

    case QVariant::String: {
        QMetaEnum meta_enum = d->meta_property.enumerator();
        int count = meta_enum.keyCount();
        QString name = data.toString();

        for (int i = 0; i < count; ++i) {
            if (QString(meta_enum.key(i)) == name) {
                current_index = i;
                break;
            }
        }

        break;
    }

    default:
        return;
        break;
    }

    d->editor->blockSignals(true);
    static_cast<QComboBox *>(d->editor)->setCurrentIndex(current_index);
    d->editor->blockSignals(false);
}

QVariant dtkComposerPropertyEditorEnum::editorData(void)
{
    return QVariant::fromValue(static_cast<QComboBox *>(d->editor)->currentIndex());
}

void dtkComposerPropertyEditorEnum::onIndexChanged(int index)
{
    int value = d->object->metaObject()->property(d->meta_property.propertyIndex()).enumerator().value(index);
    d->object->setProperty(d->meta_property.name(), QVariant::fromValue(value));
}

dtkComposerPropertyEditor *createDtkPropertyEditorEnum(const QString& property_name, QObject *object, QWidget *parent)
{
    return new dtkComposerPropertyEditorEnum(property_name, object, parent);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditor;
class dtkComposerPropertyEditorFactoryPrivate;

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorFactory declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditorFactory : public QObject
{
    Q_OBJECT

public:
    typedef dtkComposerPropertyEditor *(*dtkComposerPropertyEditorCreator)(const QString&, QObject *, QWidget *);

public:
    static dtkComposerPropertyEditorFactory *instance(void);

public:
    bool registerCreator(int type, dtkComposerPropertyEditorCreator func);

public slots:
    dtkComposerPropertyEditor *create(const QString& property_name, QObject *object, QWidget *parent = 0);

public slots:
    QList<QWidget *> createObjectProperties(QObject *object, int hierarchy_level = -1);

protected:
     dtkComposerPropertyEditorFactory(void);
    ~dtkComposerPropertyEditorFactory(void);

private:
    static dtkComposerPropertyEditorFactory *s_instance;

private:
    dtkComposerPropertyEditorFactoryPrivate *d;

private slots:
    void clear(void) {
        delete this;
    }
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorFactoryPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerPropertyEditorFactoryPrivate
{
public:
    typedef QHash<int, dtkComposerPropertyEditorFactory::dtkComposerPropertyEditorCreator> dtkComposerPropertyEditorCreatorHash;

public:
    dtkComposerPropertyEditorCreatorHash creators;
};

// ///////////////////////////////////////////////////////////////////
// Forward declarations of creators (see dtkComposerPropertyEditor.cpp for implementations).
// ///////////////////////////////////////////////////////////////////

dtkComposerPropertyEditor  *createDtkPropertyEditorDouble(const QString& property_name, QObject *object, QWidget *parent = 0);
dtkComposerPropertyEditor *createDtkPropertyEditorInteger(const QString& property_name, QObject *object, QWidget *parent = 0);
dtkComposerPropertyEditor  *createDtkPropertyEditorString(const QString& property_name, QObject *object, QWidget *parent = 0);
dtkComposerPropertyEditor    *createDtkPropertyEditorEnum(const QString& property_name, QObject *object, QWidget *parent = 0);

// ///////////////////////////////////////////////////////////////////
// dtkComposerPropertyEditorFactory implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerPropertyEditorFactory *dtkComposerPropertyEditorFactory::instance(void)
{
    if(!s_instance) {
        s_instance = new dtkComposerPropertyEditorFactory;

        s_instance->registerCreator(QMetaType::Double, createDtkPropertyEditorDouble);
        s_instance->registerCreator(QMetaType::Int, createDtkPropertyEditorInteger);
        s_instance->registerCreator(QMetaType::QString, createDtkPropertyEditorString);
        s_instance->registerCreator(-1 * static_cast<int>(QMetaType::Int), createDtkPropertyEditorEnum);
    }

    return s_instance;
}

dtkComposerPropertyEditor *dtkComposerPropertyEditorFactory::create(const QString& property_name, QObject *object, QWidget *parent)
{
    if (!object) {
        qDebug() << Q_FUNC_INFO << "Cannot create any dtkComposerPropertyEditor for null object.";
        return NULL;
    }

    const QMetaObject *meta_object = object->metaObject();
    int id = meta_object->indexOfProperty(qPrintable(property_name));
    const QMetaProperty& meta_property = meta_object->property(id);
    int type = meta_property.userType();

    if (meta_property.isEnumType())
        type *= -1;

    if (!d->creators.contains(type))
        return NULL;

    return d->creators[type](property_name, object, parent);
}

QList<QWidget *> dtkComposerPropertyEditorFactory::createObjectProperties(QObject *object, int hierarchy_level)
{
    QList<QWidget *> list;

    if (!object) {
        qDebug() << Q_FUNC_INFO << "Cannot create any dtkComposerPropertyEditor for null object.";
        return list;
    }

    const QMetaObject *meta_object = object->metaObject();

    if (meta_object) {
        int limit_level = 0;
        int offset = 0;

        while (meta_object && limit_level <= hierarchy_level) {
            meta_object = meta_object->superClass();

            if (meta_object) {
                ++limit_level;
                offset = meta_object->propertyCount();
            }
        }

        meta_object = object->metaObject();
        int count = meta_object->propertyCount();
        QString name;

        for (int i = offset; i < count; ++i) {
            name = QString(meta_object->property(i).name());
            list << reinterpret_cast<QWidget *>(dtkComposerPropertyEditorFactory::instance()->create(name, object, NULL));
        }
    }

    return list;
}

bool dtkComposerPropertyEditorFactory::registerCreator(int type, dtkComposerPropertyEditorCreator func)
{
    if(!d->creators.contains(type)) {
        d->creators.insert(type, func);

        return true;
    }

    qDebug() << Q_FUNC_INFO << QString("Unable to create dtkComposerPropertyEditor of type %1").arg(QMetaType::typeName(type));

    return false;
}

dtkComposerPropertyEditorFactory::dtkComposerPropertyEditorFactory(void) : QObject(), d(new dtkComposerPropertyEditorFactoryPrivate)
{
    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(clear()));
}

dtkComposerPropertyEditorFactory::~dtkComposerPropertyEditorFactory(void)
{
    delete d;

    d = NULL;
}

dtkComposerPropertyEditorFactory *dtkComposerPropertyEditorFactory::s_instance = NULL;

// ///////////////////////////////////////////////////////////////////
// dtkComposerToolBoxButton implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerToolBoxButton::dtkComposerToolBoxButton(QWidget *parent) : QAbstractButton(parent)
{
    this->setBackgroundRole(QPalette::Window);
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    this->setFocusPolicy(Qt::NoFocus);

    this->m_selected = false;
}

QSize dtkComposerToolBoxButton::sizeHint(void) const
{
    QSize iconSize(8, 8);

    if (!this->icon().isNull()) {
        int icone = this->style()->pixelMetric(QStyle::PM_SmallIconSize, 0, this->parentWidget() /* QToolBox */);
        iconSize += QSize(icone + 2, icone);
    }

    QSize textSize = fontMetrics().size(Qt::TextShowMnemonic, text()) + QSize(0, 8);

    QSize total(iconSize.width() + textSize.width(), qMax(iconSize.height(), textSize.height()));

    return total.expandedTo(QApplication::globalStrut());
}

QSize dtkComposerToolBoxButton::minimumSizeHint(void) const
{
    if (this->icon().isNull())
        return QSize();

    int icon = this->style()->pixelMetric(QStyle::PM_SmallIconSize, 0, this->parentWidget());

    return QSize(icon + 8, icon + 8);
}

void dtkComposerToolBoxButton::initStyleOption(QStyleOptionToolBox *option) const
{
    if (!option)
        return;

    option->initFrom(this);

    if (this->m_selected)
        option->state |= QStyle::State_Selected;

    if (this->isDown())
        option->state |= QStyle::State_Sunken;

    option->text = this->text();
    option->icon = this->icon();
}

void dtkComposerToolBoxButton::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter paint(this);
    QPainter *p = &paint;
    QStyleOptionToolBox opt;

    this->initStyleOption(&opt);
    this->style()->drawControl(QStyle::CE_ToolBoxTab, &opt, p, this->parentWidget());
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerToolBoxItemPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkComposerToolBoxItemPrivate
{
public:
    QVBoxLayout *layout;

public:
    dtkComposerToolBoxButton *button;

public:
    QWidget *widget;

public:
    bool is_expanded;
    bool is_enforced;

public:
    dtkComposerToolBox *box;
};

// ///////////////////////////////////////////////////////////////////
// dtkComposerToolBoxItem implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerToolBoxItem::dtkComposerToolBoxItem(QWidget *parent) : QFrame(parent), d(new dtkComposerToolBoxItemPrivate)
{
    d->layout = new QVBoxLayout(this);
    d->layout->setContentsMargins(0, 0, 0, 0);
    d->layout->setAlignment(Qt::AlignTop);

    d->button = new dtkComposerToolBoxButton(this);
    d->button->show();

    d->layout->addWidget(d->button);

    d->widget = NULL;
    d->is_expanded = false;
    d->is_enforced = false;
    d->box = NULL;

    this->setBackgroundRole(QPalette::Button);
    this->connect(d->button, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
}

dtkComposerToolBoxItem::~dtkComposerToolBoxItem(void)
{
    delete d;

    d = NULL;
}

bool dtkComposerToolBoxItem::isExpanded(void) const
{
    return d->is_expanded;
}

bool dtkComposerToolBoxItem::isEnforced(void) const
{
    return d->is_enforced;
}

QString dtkComposerToolBoxItem::name(void) const
{
    return d->button->text();
}

void dtkComposerToolBoxItem::showButton(void)
{
    d->button->show();
}

void dtkComposerToolBoxItem::hideButton(void)
{
    d->button->hide();
}

void dtkComposerToolBoxItem::setWidget(QWidget *widget, const QString& text, const QIcon& icon)
{
    if (!widget) {
        qDebug() << Q_FUNC_INFO << "Widget is not valid, nothing is done.";
        return;
    }

    d->button->setText(text);
    d->button->setIcon(icon);

    d->widget = widget;

    if (!d->is_expanded)
        d->widget->hide();

    d->layout->addWidget(d->widget);

    if (d->button->isVisible())
        this->resize(d->button->size() + d->widget->size());
    else
        this->resize(d->widget->size());
}

void dtkComposerToolBoxItem::setExpanded(bool expanded)
{
    d->is_expanded = expanded;

    if (!d->widget) {
        qDebug() << Q_FUNC_INFO << "No widget, nothing is done.";
        return;
    }

    if (expanded) {
        d->widget->show();
    } else {
        d->widget->hide();
    }

    d->button->setSelected(expanded);
}

void dtkComposerToolBoxItem::setEnforced(bool enforced)
{
    if (d->is_enforced == enforced)
        return;

    d->is_enforced = enforced;

    if (enforced) {
        this->disconnect(d->button, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
        this->setExpanded(true);

    } else {
        this->connect(d->button, SIGNAL(clicked()), this, SLOT(onButtonClicked()));
        this->setExpanded(d->is_expanded);
    }
}

void dtkComposerToolBoxItem::setName(const QString& name)
{
    d->button->setText(name);
}

void dtkComposerToolBoxItem::onButtonClicked(void)
{
    this->setExpanded(!d->is_expanded);

    if (d->box)
        d->box->setCurrentItem(this);
}

void dtkComposerToolBoxItem::setToolBox(dtkComposerToolBox *box)
{
    d->box = box;
}

dtkComposerToolBoxItem *dtkComposerToolBoxItem::fromObject(QObject *object, int hierarchy_level)
{
    QList<QWidget *> list = dtkComposerPropertyEditorFactory::instance()->createObjectProperties(object, hierarchy_level);

    QFrame *frame = new QFrame;

    QVBoxLayout *layout = new QVBoxLayout(frame);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(2);
    layout->setAlignment(Qt::AlignTop);

    QString name;

    for (QWidget *w : list) {
        name = QString(w->objectName()).append(":");
        layout->addWidget(new QLabel(name, frame));
        layout->addWidget(w);
    }

    frame->adjustSize();

    dtkComposerToolBoxItem *item = new dtkComposerToolBoxItem;
    item->setWidget(frame, qPrintable(object->objectName()));

    return item;
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerToolBoxPrivate declaration and implementation
// ///////////////////////////////////////////////////////////////////

class dtkComposerToolBoxPrivate
{
public:
    dtkComposerToolBox::Order order;
    dtkComposerToolBox::DisplayMode mode;

public:
    dtkComposerToolBoxItem *current_item;

public:
    typedef QList<dtkComposerToolBoxItem *> ItemList;
    ItemList items;

public:
    QFrame *frame;
    QVBoxLayout *layout;

public:
    void clear(void);
    void relayout(void);
    void insert(int index, dtkComposerToolBoxItem *item);
    void setCurrentItem(dtkComposerToolBoxItem *item);
};

void dtkComposerToolBoxPrivate::clear(void)
{
    delete this->layout;
    this->layout = NULL;

    qDeleteAll(this->items);
    this->items.clear();
}

void dtkComposerToolBoxPrivate::relayout(void)
{
    if (this->layout)
        delete this->layout;

    this->layout = new QVBoxLayout(this->frame);
    this->layout->setContentsMargins(0, 0, 0, 0);
    this->layout->setSpacing(0);
    this->layout->setAlignment(Qt::AlignTop);

    switch (this->order) {

    case dtkComposerToolBox::Ascending: {

        ItemList::ConstIterator it = items.constBegin();

        while (it != items.constEnd()) {
            this->layout->addWidget(*it);
            ++it;
        }

        break;
    }

    case dtkComposerToolBox::Descending: {

        ItemList::ConstIterator it = items.constEnd();

        while (it != items.constBegin()) {
            --it;
            this->layout->addWidget(*it);
        }

        break;
    }

    case dtkComposerToolBox::AlphaBetics: {

        QMultiMap<QString, dtkComposerToolBoxItem *> map;

        ItemList::ConstIterator it = items.constBegin();

        while (it != items.constEnd()) {
            dtkComposerToolBoxItem *item = *it;
            map.insert(item->name(), item);
            ++it;
        }

        QMultiMap<QString, dtkComposerToolBoxItem *>::ConstIterator i = map.constBegin();

        while (i != map.constEnd()) {
            this->layout->addWidget(i.value());
            ++i;
        }

        break;
    }

    default: {

        ItemList::ConstIterator it = items.constBegin();

        while (it != items.constEnd()) {
            this->layout->addWidget(*it);
            ++it;
        }

        break;
    }
    }
}

void dtkComposerToolBoxPrivate::insert(int index, dtkComposerToolBoxItem *item)
{
    switch (this->order) {

    case dtkComposerToolBox::Ascending: {

        this->layout->insertWidget(index, item);

        break;
    }

    case dtkComposerToolBox::Descending: {

        this->layout->insertWidget(this->items.count() - index, item);

        break;
    }

    case dtkComposerToolBox::AlphaBetics: {

        this->relayout();

        break;
    }

    default: {
        this->layout->insertWidget(index, item);
        break;
    }

    }
}

void dtkComposerToolBoxPrivate::setCurrentItem(dtkComposerToolBoxItem *item)
{
    if (this->current_item == item)
        this->current_item = NULL;
    else
        this->current_item = item;


    if (this->mode == dtkComposerToolBox::OneItemExpanded) {
        for (dtkComposerToolBoxItem *it : this->items) {
            it->setExpanded(false);
        }

        if (this->current_item)
            this->current_item->setExpanded(true);
    }
}

// ///////////////////////////////////////////////////////////////////
// dtkComposerToolBox implementation
// ///////////////////////////////////////////////////////////////////

dtkComposerToolBox::dtkComposerToolBox(QWidget *parent) : QScrollArea(parent), d(new dtkComposerToolBoxPrivate)
{
    d->frame = new QFrame(this);

    this->setWidget(d->frame);
    this->setWidgetResizable(true);
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setContentsMargins(0, 0, 0, 0);

    d->layout = new QVBoxLayout(d->frame);
    d->layout->setContentsMargins(0, 0, 0, 0);
    d->layout->setSpacing(0);
    d->layout->setAlignment(Qt::AlignTop);

    d->order = dtkComposerToolBox::Ascending;
    d->mode = dtkComposerToolBox::Default;
    d->current_item = NULL;
}

dtkComposerToolBox::~dtkComposerToolBox(void)
{
    delete d;

    d = NULL;
}

void dtkComposerToolBox::clear(void)
{
    d->clear();
    d->relayout();
}

int dtkComposerToolBox::count(void) const
{
    return d->items.count();
}

dtkComposerToolBox::Order dtkComposerToolBox::order(void) const
{
    return d->order;
}

dtkComposerToolBox::DisplayMode dtkComposerToolBox::displayMode(void) const
{
    return d->mode;
}

dtkComposerToolBoxItem *dtkComposerToolBox::itemAt(int index) const
{
    return d->items.at(index);
}

void dtkComposerToolBox::insertItem(int index, dtkComposerToolBoxItem *item)
{
    if (d->items.contains(item)) {
        qDebug() << Q_FUNC_INFO << "Item is already in the toolbox.";
        return;
    }

    item->setToolBox(this);
    item->setEnforced((d->mode == AllItemExpanded));
    d->items.insert(index, item);
    d->insert(index, item);
}

void dtkComposerToolBox::setCurrentItem(dtkComposerToolBoxItem *item)
{
    d->setCurrentItem(item);
}

void dtkComposerToolBox::removeItem(int index)
{
    d->layout->removeWidget(d->items.takeAt(index));
}

void dtkComposerToolBox::removeItem(dtkComposerToolBoxItem *item)
{
    this->removeItem(d->items.indexOf(item));
}

void dtkComposerToolBox::setOrder(Order order)
{
    if (d->order == order)
        return;

    d->order = order;
    d->relayout();
}

void dtkComposerToolBox::setDisplayMode(DisplayMode mode)
{
    if (d->mode == mode)
        return;

    d->mode = mode;

    bool display_all_items = false;

    switch (mode) {
    case Default:
        break;

    case AllItemExpanded:
        display_all_items = true;
        break;

    case OneItemExpanded:
        d->setCurrentItem(d->current_item);
        break;

    default:
        break;
    }

    for (dtkComposerToolBoxItem *item : d->items)
        item->setEnforced(display_all_items);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#include "dtkComposerToolBox.moc"

//
// dtkComposerToolBox.cpp ends here
