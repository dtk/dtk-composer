// Version: $Id: a7848d16dfb0e5b0aeeb406862244b24ebee360a $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerNodePrint.h"
#include "dtkComposerTransmitter.h"
#include "dtkComposerTransmitterReceiver.h"

#include "dtkComposerMetaType.h"

#include <iostream>

class dtkComposerNodePrintPrivate
{
public:
    dtkComposerTransmitterReceiverVariant receiver_stdout;
    dtkComposerTransmitterReceiverVariant receiver_stderr;

};

dtkComposerNodePrint::dtkComposerNodePrint(void) : dtkComposerNodeLeaf(), d(new dtkComposerNodePrintPrivate)
{
    this->appendReceiver(&(d->receiver_stdout));
    this->appendReceiver(&(d->receiver_stderr));
}

dtkComposerNodePrint::~dtkComposerNodePrint(void)
{
    delete d;

    d = NULL;
}

void dtkComposerNodePrint::run(void)
{
    for (QVariant v : d->receiver_stdout.allData()) {
        std::cout << qPrintable(dtk::description(v)) << std::endl;
    }

    for (QVariant v : d->receiver_stderr.allData()) {
        std::cerr << qPrintable(dtk::description(v)) <<  std::endl ;
    }
}


//
// dtkComposerNodePrint.cpp ends here
