// Version: $Id: b4e90837adae34b9fc78333cfc4c386482b387c3 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerTagCloud.h"
#include "dtkComposerTagCloudController.h"
#include "dtkComposerTagCloudList.h"
#include "dtkComposerTagCloudScope.h"
#include "dtkComposerTagCloudView.h"
#include "dtkComposerTagCloudItem.h"

class dtkComposerTagCloudControllerPrivate
{
public:
    dtkComposerTagCloud *cloud;
    dtkComposerTagCloudList *list;
    dtkComposerTagCloudScope *scope;

public:
    QList<dtkComposerTag> tags;
    QList<dtkComposerTagCloudItem> items;

public:
    QStringList filters;

public:
    bool union_mode;
};

dtkComposerTagCloudController::dtkComposerTagCloudController(void) : QObject()
{
    d = new dtkComposerTagCloudControllerPrivate;
    d->list = NULL;
    d->cloud = NULL;
    d->scope = NULL;

    d->union_mode = false;
}

dtkComposerTagCloudController::~dtkComposerTagCloudController(void)
{
    delete d;
}

void dtkComposerTagCloudController::attach(dtkComposerTagCloudView *view)
{
    d->list = view->list();
}

void dtkComposerTagCloudController::attach(dtkComposerTagCloud *cloud)
{
    d->cloud = cloud;

    connect(d->cloud, SIGNAL(tagClicked(QString)), this, SLOT(addFilter(QString)));
}

void dtkComposerTagCloudController::attach(dtkComposerTagCloudScope *scope)
{
    d->scope = scope;

    connect(d->scope, SIGNAL(tagSet(QString)), this, SLOT(setFilter(QString)));
    connect(d->scope, SIGNAL(tagAdded(QString)), this, SLOT(addFilter(QString)));
    connect(d->scope, SIGNAL(tagRemoved(QString)), this, SLOT(remFilter(QString)));
    connect(d->scope, SIGNAL(cleared(void)), this, SLOT(clear()));
    connect(d->scope, SIGNAL(unionMode(bool)), this, SLOT(onUnionMode(bool)));
}

void dtkComposerTagCloudController::addItem(QString name)
{
    d->items << dtkComposerTagCloudItem(name, "", QStringList());

    this->update();
    this->render();
}

void dtkComposerTagCloudController::addItem(QString name, QString description)
{
    d->items << dtkComposerTagCloudItem(name, description, QStringList());

    this->update();
    this->render();
}

void dtkComposerTagCloudController::addItem(QString name, QString description, QStringList tags)
{
    d->items << dtkComposerTagCloudItem(name, description, tags);

    this->update();
    this->render();
}

void dtkComposerTagCloudController::addItem(QString name, QString description, QStringList tags, QString kind, QString type)
{
    d->items << dtkComposerTagCloudItem(name, description, tags, kind, type);

    this->update();
    this->render();
}

void dtkComposerTagCloudController::update(void)
{
    // d->tags.clear();

    // QHash<QString, QStringList> tags;

    // for (dtkComposerTagCloudItem item : d->items)
    //     for(QString tag : item.tags())
    //         tags[tag] << item.name();

    // for (QString tag : tags.keys())
    //     d->tags << dtkComposerTag(tag, tags[tag].count(), tags[tag]);
}

static bool intersect(QStringList l1, QStringList l2)
{
    bool pass = true;

    for (QString s : l1) if (!l2.contains(s)) pass = false;

    return pass;
}

static bool unite(QStringList l1, QStringList l2)
{
    if (l1.isEmpty()) return true;

    bool pass = false;

    for (QString s : l1) if (l2.contains(s)) pass = true;

    return pass;
}

void dtkComposerTagCloudController::clear(void)
{
    d->filters.clear();

    this->update();
    this->render();
}

void dtkComposerTagCloudController::render(void)
{
    d->tags.clear();

    QHash<QString, QStringList> tags;

    for (dtkComposerTagCloudItem item : d->items)
        for (QString tag : item.tags())
            tags[tag] << item.name();

    for (QString tag : tags.keys())
        d->tags << dtkComposerTag(tag, tags[tag].count(), tags[tag]);

    if (d->list) {
        d->list->clear();

        for (dtkComposerTagCloudItem item : d->items)
            if ((d->union_mode && unite(d->filters, item.tags())) || (!d->union_mode && intersect(d->filters, item.tags())))
                d->list->addItem(item);
    }

    if (d->cloud) {
        d->cloud->clear();

        for (dtkComposerTag tag : d->tags)
            d->cloud->addTag(tag);

        d->cloud->render();
    }

    if (d->scope) {
        d->scope->clear();

        for (QString filter : d->filters)
            d->scope->addTag(filter, tags[filter].count());

        d->scope->render();
    }

    if (d->scope)
        d->scope->setTags(tags.keys());
}

void dtkComposerTagCloudController::addFilter(QString filter)
{
    d->filters << filter;

    this->render();
}

void dtkComposerTagCloudController::setFilter(QString filter)
{
    d->filters.clear();
    d->filters << filter;

    this->render();
}

void dtkComposerTagCloudController::remFilter(QString filter)
{
    d->filters.removeOne(filter);

    this->render();
}

void dtkComposerTagCloudController::onUnionMode(bool mode)
{
    d->union_mode = !mode;

    this->render();
}

//
// dtkComposerTagCloudController.cpp ends here
