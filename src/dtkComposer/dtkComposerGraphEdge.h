// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport>

#include <QtCore>

// /////////////////////////////////////////////////////////////////
// dtkComposerGraphEdgeList
// /////////////////////////////////////////////////////////////////

using dtkComposerGraphEdgeList = QList<class dtkComposerGraphEdge *>;

// /////////////////////////////////////////////////////////////////
// dtkComposerGraphEdge
// /////////////////////////////////////////////////////////////////

class DTKCOMPOSER_EXPORT dtkComposerGraphEdge
{
public:
     dtkComposerGraphEdge(void);
     dtkComposerGraphEdge(class dtkComposerGraphNode *s, dtkComposerGraphNode *d);
     dtkComposerGraphEdge(const dtkComposerGraphEdge&);
    ~dtkComposerGraphEdge(void);

    dtkComposerGraphEdge& operator = (const dtkComposerGraphEdge& other);

    bool operator == (const dtkComposerGraphEdge& other) const;

    dtkComposerGraphNode *source(void) const;
    dtkComposerGraphNode *destination(void) const;

    void setSource(dtkComposerGraphNode *);
    void setDestination(dtkComposerGraphNode *);

    int id(void) const;

    void setId(int id);

private:
    class dtkComposerGraphEdgePrivate *d;
};

//
// dtkComposerGraphEdge.h ends here
