// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkComposerGraphNode.h"

// ///////////////////////////////////////////////////////////////////
// dtkComposerGraphNodePrivate
// ///////////////////////////////////////////////////////////////////

class dtkComposerGraphNodePrivate
{
public:
    QString title;

    dtkComposerGraphNodeList successors;
    dtkComposerGraphNodeList predecessors;
    dtkComposerGraphNodeList childs;

    dtkComposerGraphNode::Status status = dtkComposerGraphNode::Ready;

    dtkComposerGraph *graph = nullptr;

    bool breakpoint = false;
    bool endloop_initial = false;
    bool endloop = false;

    static int count;
};

int dtkComposerGraphNodePrivate::count = 0;

// ///////////////////////////////////////////////////////////////////
// dtkComposerGraphNode
// ///////////////////////////////////////////////////////////////////

dtkComposerGraphNode::dtkComposerGraphNode(void) : QObject(), d(new dtkComposerGraphNodePrivate)
{
    d->count++;
}

dtkComposerGraphNode::~dtkComposerGraphNode(void)
{
    this->clean();
    d->graph = nullptr;

    delete d;
}

void dtkComposerGraphNode::setStatus(dtkComposerGraphNode::Status status)
{
    d->status = status;
}

dtkComposerGraphNode::Status dtkComposerGraphNode::status(void) const
{
    return d->status;
}

void dtkComposerGraphNode::clean(void)
{
    this->setStatus(dtkComposerGraphNode::Ready);
    d->endloop = d->endloop_initial;
}

dtkComposerNode *dtkComposerGraphNode::wrapee(void)
{
    return nullptr;
}

void dtkComposerGraphNode::setTitle(const QString& title)
{
    d->title = title;
    // set unique object name
    QString id = QString::number(d->count);
    id.prepend("_");
    id.prepend(title);
    id.replace(" ", "_");
    this->setObjectName(id);
}

const QString& dtkComposerGraphNode::title(void) const
{
    return d->title;
}

void dtkComposerGraphNode::addChild(dtkComposerGraphNode *node)
{
    if (!d->childs.contains(node)) {
        d->childs << node;
    }
}

void dtkComposerGraphNode::removeChild(dtkComposerGraphNode *node)
{
    d->childs.removeOne(node);
}

void dtkComposerGraphNode::addPredecessor(dtkComposerGraphNode *node)
{
    d->predecessors << node;
}

void dtkComposerGraphNode::removePredecessor(dtkComposerGraphNode *node)
{
    d->predecessors.removeOne(node);
}

void dtkComposerGraphNode::addSuccessor(dtkComposerGraphNode *node, int id)
{
    d->successors << node;
}

void dtkComposerGraphNode::removeSuccessor(dtkComposerGraphNode *node)
{
    d->successors.removeOne(node);
}

void dtkComposerGraphNode::setGraph(dtkComposerGraph *graph)
{
    d->graph = graph;
}

dtkComposerGraph *dtkComposerGraphNode::graph(void)
{
    return d->graph;
}

void dtkComposerGraphNode::setBreakPoint(bool value)
{
    d->breakpoint = value;
}

void dtkComposerGraphNode::setEndLoop(bool value)
{
    d->endloop = value;

    if (value) // endloop is set to true, keep this info in endloop_initial (used to rerun node)
        d->endloop_initial = value;
}

bool dtkComposerGraphNode::breakpoint(void) const
{
    return d->breakpoint;
}

bool dtkComposerGraphNode::endloop(void) const
{
    return d->endloop;
}

dtkComposerGraphNodeList dtkComposerGraphNode::successors(void)
{
    return d->successors;
}

dtkComposerGraphNode *dtkComposerGraphNode::firstSuccessor(void)
{
    return d->successors[0];
}

dtkComposerGraphNodeList dtkComposerGraphNode::predecessors(void)
{
    return d->predecessors;
}

dtkComposerGraphNodeList dtkComposerGraphNode::evaluableChilds(void)
{
    return d->childs;
}

dtkComposerGraphNodeList dtkComposerGraphNode::childs(void)
{
    return d->childs;
}

void dtkComposerGraphNode::eval(void)
{

}

//
// dtkComposerGraphNode.cpp ends here
